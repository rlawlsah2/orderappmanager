package com.cdef.orderappmanager.Networking;

import android.content.Context;

import com.cdef.orderappmanager.AppManagerApplication;
import com.cdef.orderappmanager.LogUtil;

import javax.inject.Inject;

/**
 * Created by kimjinmo on 2018. 2. 2..
 *
 * 사용하는 시점에서 해당 클래스를 상속받고 inject함수를 구현해준다
 */

public class NetworkUtil {

    @Inject
    protected Service service;

    protected static NetworkUtil instance = null;

    public static NetworkUtil getInstance(Context context) {
        if (NetworkUtil.instance == null)
            instance = new NetworkUtil(context);
        return instance;
    }

    protected NetworkUtil(Context context) {
//        inject(context);

        AppManagerApplication
                .getNetworkComponent(context)
                .inject(this);
    }

    public void inject(Context context){

    }

    public Service getService() {
        LogUtil.d("서비스 객체 가져오기 : " + this.service);
        return this.service;
    }



}
