package com.cdef.orderappmanager.Networking;

import android.support.annotation.Keep;

import com.cdef.orderappmanager.DataModel.OTPData;
import com.cdef.orderappmanager.Networking.Responses.LoginResponse;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by ennur on 6/25/16.
 */
public interface NetworkService {

    @Keep
    @GET("admin/api/certification")
    Observable<LoginResponse> Login();

    @Keep
    @GET
    Observable<OTPData> getOTP(@Url String url);




}
