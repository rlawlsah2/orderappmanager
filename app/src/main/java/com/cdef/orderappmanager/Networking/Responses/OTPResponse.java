package com.cdef.orderappmanager.Networking.Responses;

import com.cdef.orderappmanager.DataModel.OTPData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */

public class OTPResponse extends BaseResponse {


    @SerializedName("result")
    @Expose
    public OTPData result = new OTPData();
}
