package com.cdef.orderappmanager.Networking;


import com.cdef.orderappmanager.DataModel.OTPData;
import com.cdef.orderappmanager.Networking.Responses.BaseResponse;
import com.cdef.orderappmanager.Networking.Responses.LoginResponse;
import com.cdef.orderappmanager.Utils.ErrorUtils;

import rx.Observable;


/**
 * Created by ennur on 6/25/16.
 */
public class Service {
    private final NetworkService networkService;

    public NetworkService getNetworkService() {
        return this.networkService;
    }

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }


    public Observable<LoginResponse> login() {

        return this.networkService.Login();
    }

    public Observable<OTPData> getOTP(String url) {

        return this.networkService.getOTP(url);
    }


    public static boolean requestErrorHandler(BaseResponse Response) {
        if (Response != null) {
            return ErrorUtils.isSuccess(Response.getStatus().code);
        } else
            return false;
    }

}
