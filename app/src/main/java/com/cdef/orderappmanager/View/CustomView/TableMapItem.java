package com.cdef.orderappmanager.View.CustomView;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.databinding.LayoutTableMapItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class TableMapItem extends RelativeLayout {

    LayoutTableMapItemBinding binding;

    Context mContext;

    private eventListener listener;

    public TableMapItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public TableMapItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        registerHandler();

    }

    public TableMapItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        registerHandler();

    }


    private void registerHandler() {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_table_map_item, this, true);

//        LayoutInflater.from(getContext()).inflate(R.layout.layout_table_map_item, this, true);
//        ButterKnife.bind(this);
    }

    public void setListener(eventListener listener) {
        this.listener = listener;
    }


    public interface eventListener {
        void countChange();
    }

    public void setData(com.cdef.orderappmanager.DataModel.Login.TableMapItem data) {
        binding.setData(data);
    }

    public com.cdef.orderappmanager.DataModel.Login.TableMapItem getData() {
        return binding.getData();
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.title.setSelected(selected);
        this.binding.textTableNo.setSelected(selected);
    }
}
