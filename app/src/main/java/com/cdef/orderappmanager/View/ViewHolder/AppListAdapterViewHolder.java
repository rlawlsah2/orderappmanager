package com.cdef.orderappmanager.View.ViewHolder;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cdef.orderappmanager.databinding.AdapterApplistItemBinding;


/**
 * Created by kimjinmo on 2018. 4. 30..
 */


public class AppListAdapterViewHolder extends RecyclerView.ViewHolder {


    public AdapterApplistItemBinding binding;

    public AppListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
