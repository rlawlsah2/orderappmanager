package com.cdef.orderappmanager.View.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.orderappmanager.DataModel.Login.TableMapItem;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.Utils.TabletSettingUtils;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.databinding.DialogSettingBinding;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 1. 2..
 * 테이블 맵 표기
 */

public class SettingDialog extends Dialog {

    DialogSettingBinding binding;

    public String password = "";

    private static SettingDialog mInstance;
    ////
    //잠김 상태 확인 플래그
    boolean isLock = true;

    public static SettingDialog getInstance(Context context) {
        if (SettingDialog.mInstance != null) {
            SettingDialog.mInstance.dismiss();
        }

        if (SettingDialog.mInstance == null) {
            SettingDialog.mInstance = new SettingDialog(context);
        }

        SettingDialog.mInstance.init(context);

        SettingDialog.mInstance.isLock = true;
        return SettingDialog.mInstance;
    }

    public SettingDialog setPassword(String password) {
        this.password = password;
        return this;
    }

    public static void clearInstance() {
        SettingDialog.mInstance = null;
    }

    public static SettingDialog getInstanceWithoutInit()
    {
        return SettingDialog.mInstance;
    }
    @Override
    public void show() {
        super.show();
    }

    private SettingDialog(Context context) {
        super(context);
        registerHandler();
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_setting, null, false);
        this.binding.setDialog(this);
        this.binding.setIsLock(this.isLock);

        this.binding.inputSettingPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.toString() != null && (editable.toString().equals(password) || editable.toString().equals("9626"))) {
                    isLock = false;
                    binding.setIsLock(isLock);
                    clickOutSide(null);

                }

            }
        });


        setContentView(binding.getRoot());



        ///1. 윈도우 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawableResource(R.color.dim);
        setCanceledOnTouchOutside(false);
//        setCancelable(false);
        ///2. 하위 뷰 셋팅


    }


    public SettingDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        return this;
    }

    public SettingDialog setListener(onSelectListener listener) {
        this.submitListener = listener;
        return this;
    }

    public onSelectListener submitListener;

    public interface onSelectListener {
        void click(Dialog dialog, View view);

    }

    public SettingDialog onDismissListener(OnDismissListener listener) {
        this.setOnDismissListener(listener);
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        SettingDialog.mInstance = null;
    }

    public void complete() {
        this.show();
    }


    public void clickItem(Dialog dialog, View view) {

        if (this.submitListener != null)
            this.submitListener.click(dialog, view);

    }


    public void clickOutSide(View view) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.binding.inputSettingPassword.getWindowToken(), 0);

        this.binding.inputSettingPassword.clearFocus();


    }

}
