package com.cdef.orderappmanager.View.Activity;

import android.Manifest;
import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.orderappmanager.DataModel.Login.AppInfo;
import com.cdef.orderappmanager.Exceptions.CannotLoginExceptions;
import com.cdef.orderappmanager.Exceptions.RestartExceptions;
import com.cdef.orderappmanager.Exceptions.UserNotFoundExceptions;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.Utils.DefaultExceptionHandler;
import com.cdef.orderappmanager.Utils.PreferenceUtils;
import com.cdef.orderappmanager.Utils.QueueUtils;
import com.cdef.orderappmanager.Utils.TabletSettingUtils;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.View.Adapter.AppListAdapter;
import com.cdef.orderappmanager.View.Dialog.DownloaderDialog;
import com.cdef.orderappmanager.View.Dialog.MapDialog;
import com.cdef.orderappmanager.View.Dialog.SettingDialog;
import com.cdef.orderappmanager.View.ViewHolder.AppListAdapterViewHolder;
import com.cdef.orderappmanager.ViewModel.LoginViewModel;
import com.cdef.orderappmanager.databinding.ActivityMain2Binding;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.tedpark.tedpermission.rx2.TedRx2Permission;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * 앱이 켜지면
 * <p>
 * 1. 로그인 여부 검사
 * 2. 테이블 번호 선택 검사
 * 3. 홈화면
 **/
public class MainActivity2 extends BaseActivity {


    HashSet<Integer> downloadIdSet = new HashSet<>();   //다운로드 아이디 셋

    LoginViewModel loginViewModel;
    ActivityMain2Binding binding;
    public static final int RC_SIGN_IN = 991;   //google acount result value

    private WifiManager wifiManager = null;
    CompositeSubscription compositeSubscription = new CompositeSubscription();

    GoogleApiClient mGoogleApiClient;
    AppListAdapter appListAdapter;

    RequestOptions requestOptions;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        setUI();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestOptions = new RequestOptions();
        this.requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false);
        this.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));

        this.loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        this.loginViewModel.initRepository(this);

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main2);
        this.binding.setActivity(this);
        binding.layoutCoverDisconnected.setVisibility(View.GONE);

        ///앱리스트 가로 리스트모드로 셋팅
        this.binding.recyclerViewAppList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ///가운데 영역 리사이징
        this.binding.imageMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (binding.imageMain.getMeasuredHeight() > 2) {

                    ///이미지 메인
                    binding.imageMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) binding.imageMain.getLayoutParams();
                    lp.width = (int) (binding.imageMain.getMeasuredHeight() * (946 / 332.0));
                    binding.imageMain.setLayoutParams(lp);


                    ///테이블 번호
                    LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) binding.textTableNo.getLayoutParams();
                    lp2.width = lp.width;
                    binding.textTableNo.setLayoutParams(lp2);


                    ///설치된 앱 목록
                    LinearLayout.LayoutParams lp3 = (LinearLayout.LayoutParams) binding.recyclerViewAppList.getLayoutParams();
                    lp3.width = lp.width;
                    binding.recyclerViewAppList.setLayoutParams(lp3);
                }
            }
        });
        initDonwloadModule();
        setUI();
        showVersionCode();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            this.compositeSubscription.add(googleLogin(task)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(throwable -> Observable.error(throwable))
                    .flatMap(account -> {
                        if (account == null) {
                            return Observable.error(new UserNotFoundExceptions("유저없음"));
                        } else
                            return goHoeatLogin(account);
                    })
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        initView();
                    }, throwable -> {
                        LogUtil.d("뭐땜시 에러가 뜨냐1 : " + throwable);
                        LogUtil.d("뭐땜시 에러가 뜨냐2: " + throwable.toString());
                        ((Throwable) throwable).printStackTrace();
                        if ((throwable instanceof UserNotFoundExceptions) || (throwable instanceof CannotLoginExceptions)) {
                            ///호잇에 등록된게 없는 계정.
                            Toast.makeText(this, "호잇에 등록되지않은 계정입니다. 등록후 이용해주세요.", Toast.LENGTH_SHORT).show();
                            this.binding.layoutCoverLoginRetry.setVisibility(View.VISIBLE);
                            this.binding.layoutCoverLoginRetry.bringToFront();
                        } else if (throwable instanceof IOException) {
                            Toast.makeText(this, "서버에 연결할 수 없습니다.", Toast.LENGTH_SHORT).show();
                        }

                    }, () -> {

                    }));


        }
    }


    private Observable<GoogleSignInAccount> googleLogin(Task<GoogleSignInAccount> completedTask) {
        GoogleSignInAccount account = null;
        try {
            account = completedTask.getResult(ApiException.class);
        } catch (ApiException e) {
            account = null;
            return Observable.error(e);
        }
        return Observable.just(account);
    }

    private Observable<GoogleSignInAccount> checkAlreadyGoogleLogin() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        return Observable.just(account);

    }

    public Observable goHoeatLogin(GoogleSignInAccount account) {
        if (account == null)
            return Observable.error(new NullPointerException("구글 계정 정보 없음"));
        else {
            try {
                LogUtil.d("사용자 정보 getDisplayName: " + account.getDisplayName());
            } catch (Exception e) {
            }
            try {
                LogUtil.d("사용자 정보 getEmail: " + account.getEmail());
            } catch (Exception e) {
            }
            try {
                LogUtil.d("사용자 정보 token: " + account.getIdToken());
            } catch (Exception e) {
            }
            TabletSettingUtils.getInstacne().setAccessToken(account.getIdToken());
            TabletSettingUtils.getInstacne().setEncodedEmail(account.getEmail());


            return this.loginViewModel.login()
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(loginData -> {
                        this.binding.setBranchInfo(loginData.branchInfo);
                        TabletSettingUtils.getInstacne().setTableMaps(loginData.tableMap);
//                        LogUtil.d("로그인 결과 : " + loginData.tableMap.get(0).talbeMapJson.get(0).tNo);

                        return Observable.just(true);
                    });
        }
    }

    /**
     * 화면 중산단에 표시되는 테이블 번호
     **/
    private void setTableNo() {
        StringBuilder tmp = new StringBuilder();
        tmp.append("<font color=\"#cccccc\" font-weight=\"lighter\">");
        tmp.append("현재 테이블 번호는")
                .append("</font>")
                .append("  <h1><strong><font color=\"#ffffff\">")
                .append(TabletSettingUtils.getInstacne().getSelectedTableHallName().toUpperCase())
                .append(" - ")
                .append(TabletSettingUtils.getInstacne().getSelectedTableNo().toUpperCase())
                .append("</font></strong></h1>  ")
                .append("<font color=\"#cccccc\"font-weight=\"lighter\">")
                .append("입니다")
                .append("</font>");


        LogUtil.d("테이블 번호 표시 : " + tmp.toString());
        this.binding.textTableNo.setText(Html.fromHtml(tmp.toString()));
    }

    /**
     * 로그인 성공 직후에 호출
     */
    private void initView() {
        ///로그인 된 정보를 바탕으로 초기화 셋팅이 필요하다.
        ///1. 로그인 branchId 비교
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
            if (TabletSettingUtils.getInstacne().getLoginBranchId() == null || !TabletSettingUtils.getInstacne().getLoginBranchId().equals(TabletSettingUtils.getInstacne().getBranchInfo().branchId)) {
                TabletSettingUtils.getInstacne().clearAllLocalData();
                TabletSettingUtils.getInstacne().setLoginBranchId(TabletSettingUtils.getInstacne().getBranchInfo().branchId);
            }
        }

        ///2. 로그인 - 선택된 테이블이 있는지 확인
        if (TabletSettingUtils.getInstacne().getSelectedTableHallName() != null && TabletSettingUtils.getInstacne().getSelectedTableNo() != null) {
            setTableNo();

        } else {
            //확실히 하기위해 다 초기화 해버리고 선택 화면을 띄워버리자
            TabletSettingUtils.getInstacne().setSelectedTableHallName(null);
            TabletSettingUtils.getInstacne().setSelectedTableNo(null);
            Toast.makeText(getApplicationContext(), "해당 기기의 테이블 좌석을 선택해주세요.", Toast.LENGTH_SHORT).show();
            goSelectTableNo();
        }

        ///3. 앱 목록을 파베로부터 가져온다
        if (loginViewModel != null) {
            loginViewModel.getFBRefrence()
                    .child("apps")
                    .addValueEventListener(getAppsValueListner);

            ///앱 목록 셋팅
            if (this.appListAdapter == null) {
                this.appListAdapter = new AppListAdapter(AppInfo.class, R.layout.adapter_applist_item, AppListAdapterViewHolder.class, loginViewModel.getFBRefrence()
                        .child("apps")
                        .orderByChild("appType").equalTo("app")
                );

                this.appListAdapter.registerAdapterDataObserver(adapterDataObserver);
                this.appListAdapter.setRequestManager(Glide.with(this));
                this.appListAdapter.setListener((appInfo -> {
                    Toast.makeText(getApplicationContext(), "선택한 항목 : " + appInfo.appName, Toast.LENGTH_SHORT).show();
                    try {
                        String app = appInfo.pkgName;
                        Intent intent = getPackageManager().getLaunchIntentForPackage(app);
                        if (intent != null) {
                            startActivity(intent);
                        } else {
//                            Toast.makeText(getApplicationContext(), "설치되어 있지않습니다", Toast.LENGTH_SHORT).show();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + app)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + app)));
                            }

                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "설치되어 있지않습니다", Toast.LENGTH_SHORT).show();
                    }
                }));
            }

            this.binding.recyclerViewAppList.setAdapter(this.appListAdapter);

//                    .addListenerForSingleValueEvent(getAppsValueListner);
        }


        ///4. 기본이미지 셋팅하기
        ///(1). 이미지 로컬에 저장하기
        try {

            if (TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.background != null)
                TabletSettingUtils.getInstacne().setDefaultBackgroundImage(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.background);
            Glide.with(this)
                    .load(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.background)
                    .apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            binding.layoutMain.setBackground(resource);
                        }
                    });
        } catch (Exception e) {
            Glide.with(this)
                    .load(TabletSettingUtils.getInstacne().getDefaultBackgroundImage())
                    .apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            binding.layoutMain.setBackground(resource);
                        }
                    });

        }
        try {

            if (TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.buttonArea != null)
                TabletSettingUtils.getInstacne().setDefaultStarterImage(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.buttonArea);
            Glide.with(this)
                    .load(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.appManager.buttonArea)
                    .apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            binding.imageMain.setBackground(resource);
                        }
                    });
        } catch (Exception e) {
            Glide.with(this)
                    .load(TabletSettingUtils.getInstacne().getDefaultStarterImage())
                    .apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            binding.imageMain.setBackground(resource);
                        }
                    });

        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null)
            this.mGoogleApiClient.connect();
        compositeSubscription.add(checkAlreadyGoogleLogin()
                .subscribeOn(Schedulers.computation())
                .flatMap(account -> {
                    if (account == null) {
                        return Observable.error(new UserNotFoundExceptions("유저없음"));
                    } else
                        return goHoeatLogin(account);
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    initView();
                }, throwable -> {
                    LogUtil.d("뭐땜시 에러가 뜨냐1 : " + throwable);
                    LogUtil.d("뭐땜시 에러가 뜨냐2: " + throwable.toString());
                    ((Throwable) throwable).printStackTrace();
                    if ((throwable instanceof UserNotFoundExceptions) || (throwable instanceof CannotLoginExceptions)) {
                        ///호잇에 등록된게 없는 계정.
                        Toast.makeText(this, "호잇에 등록되지않은 계정입니다. 등록후 이용해주세요.", Toast.LENGTH_SHORT).show();
                        this.binding.layoutCoverLoginRetry.setVisibility(View.VISIBLE);
                        this.binding.layoutCoverLoginRetry.bringToFront();
                    } else if (throwable instanceof IOException) {
                        Toast.makeText(this, "서버에 연결할 수 없습니다.", Toast.LENGTH_SHORT).show();
                    }

                }, () -> {

                }));

    }

    @Override
    protected void onPause() {
        super.onPause();
        this.stopWifiCheck();
        this.stopBTCheck();
        this.stopBatteryCheck();
//        this.compositeSubscription.clear();

        ///파베 앱 목록에 걸어둔 리스너를 해제
        try {
            loginViewModel.getFBRefrence()
                    .child("apps")
                    .removeEventListener(getAppsValueListner);

//            if (this.appListAdapter != null) {
//                this.appListAdapter.cleanup();
//                this.appListAdapter = null;
//            }


        } catch (Exception e) {

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        this.startWifiCheck();
        this.startBTCheck();
        this.startBatteryCheck();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_INSTALL);
        intentFilter.addDataScheme("package");
        registerReceiver(installReceiver, intentFilter);
    }


    //
//    ///권한 체크 기능 추가
//    ///1. 위치
//    ///2. 블루투스
//    ///3. 저장소
//
//    public Observable checkPermission()
//    {
//
//        TedRx2Permission.with(this)
//                .setRationaleTitle("위치 정보 추가")
//                .setRationaleMessage("위치 정보를 추가하시면 현재 내 거리로부터 가까운 정보를 받아보실 수 있습니다.") // "we need permission for read contact and find your location"
//                .setDeniedTitle("위치 정보를 배제한 정보가 표시됩니다.")
//                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
//                .setGotoSettingButton(true)
//                .request()
//                .subscribe(tedPermissionResult -> {
//                    LogUtil.d("위치정보 얻어오기: " + tedPermissionResult.isGranted());
//                }, throwable -> {
//                    LogUtil.d("갑분에 : " + throwable);
//                    LogUtil.d("갑분에 : " + throwable.getStackTrace());
//                }, () -> {
//
//                });
//
//    }

    /**
     * CheckBattery - start
     */
    private void startBatteryCheck() {
        IntentFilter batteryFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        this.registerReceiver(batteryReceiver, batteryFilter);
    }

    /**
     * CheckBattery - stop
     */
    private void stopBatteryCheck() {
        this.unregisterReceiver(batteryReceiver);
    }


    /**
     * CheckWifi - start
     */
    private void startWifiCheck() {
        compositeSubscription.add(Observable.just(wifiManager)
                .flatMap(wifiManager1 -> {
                    if (this.wifiManager == null) {
                        this.wifiManager = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE));
                    }
                    return Observable.just(this.wifiManager);
                })
                .subscribeOn(Schedulers.io())
                .flatMap(wifiManager1 -> {
                    // wifi 수신감도 체크 리시버 등록
                    IntentFilter rssiFilter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
                    this.registerReceiver(wifiChangeReceiver, rssiFilter);
                    this.wifiManager.startScan();
                    return Observable.just(true);
                }).subscribe(result -> {
                }, error -> {
                })
        );
    }

    /**
     * CheckWifi - stop
     */
    private void stopWifiCheck() {
        this.unregisterReceiver(wifiChangeReceiver);
    }


    /**
     * CheckBT - start
     */
    private void startBTCheck() {

        IntentFilter btFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        this.registerReceiver(btChangeReceiver, btFilter);
        checkBTState();

    }

    /**
     * CheckBT - stop
     */
    private void stopBTCheck() {
        this.unregisterReceiver(btChangeReceiver);
    }

    public void goLogin() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.server_client_id))
                .build();
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void luanchHOEAT() {
        try {
            if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
                String orderAppPackageName = getOrderApp().pkgName;
                Intent intent = getPackageManager().getLaunchIntentForPackage(orderAppPackageName);
                if (intent != null) {
                    intent.putExtra("apiKey", TabletSettingUtils.getInstacne().getApiKey());
                    intent.putExtra("tableNo", TabletSettingUtils.getInstacne().getSelectedTableNo());
                    intent.putExtra("branchName", TabletSettingUtils.getInstacne().getBranchInfo().branchName);
                    LogUtil.d("주문앱 실행시 인텐트 : " + intent.getExtras());
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "주문앱이 설치되어 있지않습니다. 오른쪽 상단의 업데이트를 눌러 설치해주세요.", Toast.LENGTH_SHORT).show();
                }


            }
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "주문앱이 설치되어 있지않습니다. 오른쪽 상단의 업데이트를 눌러 설치해주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 블루투스 상태체크 리시버
     **/
    private BroadcastReceiver btChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            checkBTState();
        }
    };

    private void checkBTState() {
        compositeSubscription.add(
                Observable.just(BluetoothAdapter.getDefaultAdapter())
                        .flatMap(bluetoothAdapter -> {
                            LogUtil.d("@@ btChangeReceiver : " + bluetoothAdapter);
                            if (bluetoothAdapter == null)
                                return Observable.error(new Exception("bluetooth not found"));
                            else {
                                return Observable.just(bluetoothAdapter);
                            }
                        })
                        .subscribe(bluetoothAdapter -> {
                            if (bluetoothAdapter.isEnabled()) {
                                binding.setStateBT(1);
                            } else {
                                binding.setStateBT(0);
                            }

                        }, throwable -> {
                        })


        );
    }


    /**
     * 와이파이 상태체크 리시버
     **/
    private BroadcastReceiver wifiChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            WifiManager wifiMan = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiMan.startScan();
            int numberOfLevels = 5;
            WifiInfo wifiInfo = wifiMan.getConnectionInfo();
            int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
            LogUtil.d("와이파이 신호를 탐색합니다 : " + level);

            binding.setStateWifi((wifiManager != null && wifiManager.isWifiEnabled()));

            if (wifiManager != null && wifiManager.isWifiEnabled()) {
                if (level >= 4) {
                    binding.imgWifiState.setBackgroundResource(R.drawable.btn_wifi_on3);
                } else if (level >= 2) {
                    binding.imgWifiState.setBackgroundResource(R.drawable.btn_wifi_on2);
                } else if (level >= 1) {
                    binding.imgWifiState.setBackgroundResource(R.drawable.btn_wifi_on1);
                } else {
                    binding.imgWifiState.setBackgroundResource(R.drawable.btn_wifi_on0);
                }
                binding.layoutCoverDisconnected.setVisibility(View.GONE);

            } else {
                binding.imgWifiState.setBackgroundResource(R.drawable.btn_wifi_off);
                binding.layoutCoverDisconnected.bringToFront();
                binding.layoutCoverDisconnected.setVisibility(View.VISIBLE);
            }
        }
    };


    /**
     * 배터리 상태 체크
     **/
    private BroadcastReceiver batteryReceiver =
            new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {

                    String action = intent.getAction();
                    LogUtil.d("@@ batteryReceiver : " + action);

                    if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
                        int health = intent.getIntExtra("health", BatteryManager.BATTERY_HEALTH_UNKNOWN);
                        int level = intent.getIntExtra("level", 0);
                        int plug = intent.getIntExtra("plugged", 0);
                        int scale = intent.getIntExtra("scale", 100);
                        int status = intent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
                        String technology = intent.getStringExtra("technology");
                        int temperature = intent.getIntExtra("temperature", 0);
                        int voltage = intent.getIntExtra("voltage", 0);

                        // 배터리 잔량 확인

                        // 충전 방식
                        if (plug == 0) {
                            binding.setStateCharge(false);
                            binding.setChargeValue((int) ((level / (float) scale) * 100));
                        } else {
                            binding.setStateCharge(true);
                        }

                    }
                }
            };


    public void goSelectTableNo() {
        MapDialog.getInstance(this)
                .setSelectedTableNo(TabletSettingUtils.getInstacne().getSelectedTableHallName(), TabletSettingUtils.getInstacne().getSelectedTableNo())
                .setListener(
                        (hallName, selectedTable) -> {
                            TabletSettingUtils.getInstacne().setSelectedTableHallName(hallName);
                            TabletSettingUtils.getInstacne().setSelectedTableNo(selectedTable.tNo);
                            MapDialog.getInstance(this).dismiss();
                            setTableNo();
                            Toast.makeText(this, "변경되었습니다.", Toast.LENGTH_SHORT).show();
                        }).complete();
    }

    public void openWifiSetting() {
        Intent intent;
        intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivity(intent);
        Toast.makeText(getApplicationContext(), R.string.toast_open_setting, Toast.LENGTH_SHORT).show();
    }

    public void goSetting() {


        SettingDialog.getInstance(this)
                .setListener((dialog, view) -> {

                    Intent intent;
                    switch (view.getId()) {
                        case R.id.settingItem1:
                            openWifiSetting();
                            break;
                        case R.id.settingItem2:
                            intent = new Intent(Settings.ACTION_DISPLAY_SETTINGS);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), R.string.toast_open_setting, Toast.LENGTH_SHORT).show();
                            break;
//                        case R.id.settingItem6:
//                            ImageDialogBuilder.getInstance(this).setImage(R.drawable.img_light_example)
//                                    .setOkButton("설정하기", view1 -> {
//                                        Intent sIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
//                                        startActivity(sIntent);
//                                        Toast.makeText(getApplicationContext(), R.string.toast_open_setting, Toast.LENGTH_SHORT).show();
//                                        ImageDialogBuilder.getInstance(this).dismiss();
//                                    }).setCancelButton()
//                                    .complete();
//
//
//                            break;
                        case R.id.settingItem3:
                            goSelectTableNo();
                            Toast.makeText(getApplicationContext(), R.string.toast_open_setting, Toast.LENGTH_SHORT).show();
                            break;
                        case R.id.settingItem4:
                            AlertDialog alertDialog = new AlertDialog.Builder(this)
                                    .setTitle(getString(R.string.alert))
                                    .setMessage(getString(R.string.alert_logout))
                                    .setPositiveButton(R.string.ok, ((dialogInterface, i) -> {

                                        PreferenceUtils.clearPref(this, PreferenceUtils.PREF_FILENAME_TABLESETTING);

                                        if (dialog != null)
                                            dialog.dismiss();
                                        this.binding.layoutCoverLoginRetry.bringToFront();
                                        this.binding.layoutCoverLoginRetry.setVisibility(View.VISIBLE);
                                        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                                                .setResultCallback((ResultCallback<Status>) status -> {
                                                    Toast.makeText(getApplicationContext(), "로그아웃 완료. 자동으로 재시작 됩니다.", Toast.LENGTH_SHORT).show();
                                                    ///
                                                    throw new NullPointerException("로그아웃");
                                                });
                                    }))
                                    .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {

                                    })
                                    .create();
                            alertDialog.show();
                            break;

                        case R.id.settingItem5:
                            throw new NullPointerException("재시작");
                    }
                })
                .complete();


        this.loginViewModel.getOTP(QueueUtils.getOTP(TabletSettingUtils.getInstacne().getBranchInfo().posIp))

                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(otpData -> {
                    SettingDialog.getInstanceWithoutInit()
                            .setPassword((otpData != null && otpData.password != null) ? otpData.password : "9626");


                }, throwable -> {

                    SettingDialog.getInstanceWithoutInit()
                            .setPassword("9626");
                });
    }


    ////다운로드 모듈 관련 함수들
    public void initDonwloadModule() {

        // Setting timeout globally for the download network requests:
        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(1800_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);
    }

    /**
     * fb로부터 가져온 앱 목록을 저장
     */
    private void updateAppList(DataSnapshot dataSnapshot) {
        LogUtil.d("앱 체크 updateAppList : " + dataSnapshot);
        HashMap<String, AppInfo> list = new HashMap<>();
        AppInfo tmp;
        for (DataSnapshot item : dataSnapshot.getChildren()) {
            tmp = item.getValue(AppInfo.class);
            tmp.key = item.getKey();
            list.put(item.getKey(), tmp);
        }
        LogUtil.d("앱 체크 list : " + list.size());

        TabletSettingUtils.getInstacne().setRequireAppList(list);
        updateOrderAppView();
    }

    /**
     * 가져온 앱 목록으로부터 필요한 뷰들을 갱신하는 과정이 필요하다.
     * 1. 주문앱 확인후 버전 체크
     * 2. 앱매니저 확인
     * 3. 다른 앱은 여기서 확인할 필요가 없쥬?
     **/
    private void updateOrderAppView() {

        this.binding.layoutUpdate.bringToFront();

        ///1. 주문앱 체크
        AppInfo orderApp = getOrderApp();
        if (orderApp != null && Utils.isNeedUpdate(orderApp)) {
            ///로컬에 깔려있는지 확인
            this.binding.layoutButtonUpdateOrderApp.setVisibility(View.VISIBLE);

        } else {
            this.binding.layoutButtonUpdateOrderApp.setVisibility(View.GONE);
        }

        ///2. 앱매니저 체크
        AppInfo appManager = getAppManager();
        if (appManager != null && Utils.isNeedUpdate(appManager)) {
            ///로컬에 깔려있는지 확인
            this.binding.layoutButtonUpdateAppManager.setVisibility(View.VISIBLE);
        } else {
            this.binding.layoutButtonUpdateAppManager.setVisibility(View.GONE);
        }


    }

    /***
     *  TabletSettingUtils 목록에서 주문앱 정보 가져오기
     * **/
    private AppInfo getOrderApp() {
        for (String key : TabletSettingUtils.getInstacne().getRequireAppList().keySet()) {
            LogUtil.d("앱 체크 getOrderApp key: ------   " + key);
            try {
                if (TabletSettingUtils.getInstacne().getRequireAppList().get(key).appType != null && TabletSettingUtils.getInstacne().getRequireAppList().get(key).appType.equals("orderApp")) {
                    return TabletSettingUtils.getInstacne().getRequireAppList().get(key);
                }
            } catch (Exception e) {

            }
        }
        return null;

    }

    /***
     *  TabletSettingUtils 목록에서 앱매니저 정보 가져오기
     * **/
    private AppInfo getAppManager() {
        for (String key : TabletSettingUtils.getInstacne().getRequireAppList().keySet()) {
            LogUtil.d("앱 체크 getOrderApp key: ------   " + key);
            try {
                if (TabletSettingUtils.getInstacne().getRequireAppList().get(key).appType != null && TabletSettingUtils.getInstacne().getRequireAppList().get(key).appType.equals("launcher")) {
                    return TabletSettingUtils.getInstacne().getRequireAppList().get(key);
                }
            } catch (Exception e) {

            }
        }
        return null;

    }


    ///1회 검색용
    ValueEventListener getAppsValue = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            if (dataSnapshot != null && dataSnapshot.getValue() != null) {
                updateAppList(dataSnapshot);
            }


        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
    //리스너
    ValueEventListener getAppsValueListner = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            ///한번 더 긁어와서 뷰를 갱신하자
            loginViewModel.getFBRefrence()
                    .child("apps")
                    .addListenerForSingleValueEvent(getAppsValue);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    ///주문앱 확인
    public void checkAppList() {


        if (loginViewModel != null)
            loginViewModel.getFBRefrence().child("apps")
                    .addListenerForSingleValueEvent(getAppsValue);

    }

    /**
     * 주문앱 업데이트
     **/
    public void goOrderAppUpdate() {
        if (getOrderApp() != null) {
            if (getOrderApp().sourceType != null && getOrderApp().sourceType.equals("playStore")) {
                ///앱스토어로 보내자
                final String appPackageName = getOrderApp().pkgName;
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            } else {
                ///다운로드를 하자
                TedRx2Permission.with(this)
                        .setRationaleTitle(R.string.permission_title)
                        .setRationaleMessage(R.string.permission_storage) // "we need permission for read contact and find your location"
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .request()
                        .subscribe(tedPermissionResult -> {
                            if (tedPermissionResult.isGranted()) {

                                DownloaderDialog.getInstance(this)
                                        .setURL(getOrderApp().url)
                                        .setFileName(getOrderApp().key + "_" + Utils.getDate("yyyyMMdd") + ".apk")
                                        .complete();
                            } else {
                                Toast.makeText(this,
                                        getString(R.string.no_permission), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }, throwable -> {

                        });

            }
        } else {
            Toast.makeText(getApplicationContext(), "주문앱 정보를 찾을 수 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 앱매니저 업데이트
     **/
    public void goAppManagerUpdate() {


        if (getAppManager() != null) {
            if (getAppManager().sourceType != null && getAppManager().sourceType.equals("playStore")) {
                ///앱스토어로 보내자
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            } else {
                ///다운로드를 하자
                TedRx2Permission.with(this)
                        .setRationaleTitle(R.string.permission_title)
                        .setRationaleMessage(R.string.permission_storage) // "we need permission for read contact and find your location"
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .request()
                        .subscribe(tedPermissionResult -> {
                            if (tedPermissionResult.isGranted()) {
                                DownloaderDialog.getInstance(this)
                                        .setURL(getAppManager().url)
                                        .setFileName(getAppManager().key + "_" + Utils.getDate("yyyyMMdd") + ".apk")
                                        .complete();
                            } else {
                                Toast.makeText(this,
                                        getString(R.string.no_permission), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }, throwable -> {

                        });


            }
        } else {
            Toast.makeText(getApplicationContext(), "매니저 앱 정보를 찾을 수 없습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 앱 설치에 대한 리시버
     */

    BroadcastReceiver installReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
//                Uri data = intent.getData();
//                String packageName = data.getEncodedSchemeSpecificPart();
//
//
//
//                LogUtil.d("The installed package is: " + packageName);

                //앱 설치가 일어났으니 뷰 한번씩 갱신해주자
                //1. 업데이트 관련
                checkAppList();

            }
        }
    };


    public void showVersionCode() {
        if (Utils.getPackageInfo(getPackageName()) != null) {
            this.binding.setVersion(Utils.getPackageInfo(getPackageName()).versionName + "(" + Utils.getPackageInfo(getPackageName()).versionCode + ")");
            LogUtil.d("버전 코드 확인 : " + this.binding.getVersion());
        }
    }

    /**
     * 미등록 계정인 경우 로그아웃 후 재 로그인해야함.
     **/
    public void logoutAndRetry() {
        this.binding.layoutCoverLoginRetry.setVisibility(View.GONE);


        Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        LogUtil.d("로그아웃 상황 : " + status.getStatusMessage());
                        goLogin();
                    }
                });

    }

    ///앱 목록 표시하기위한 리스너
    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
        }
    };

}
