package com.cdef.orderappmanager.View.CustomView;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.databinding.LayoutSettingItemBinding;
import com.cdef.orderappmanager.databinding.LayoutTableMapItemBinding;

/**
 * Created by kimjinmo on 2018. 8. 23..
 */

public class SettingItem extends RelativeLayout {

    LayoutSettingItemBinding binding;
    String title, subTitle;

    public SettingItem(Context context) {
        super(context);
        registerHandler(null);
    }

    public SettingItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        registerHandler(attrs);

    }

    public SettingItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        registerHandler(attrs);

    }


    private void registerHandler(AttributeSet attrs) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_setting_item, this, true);

        if (attrs != null) {
            title = getContext().obtainStyledAttributes(attrs, R.styleable.Setting)
                    .getString(R.styleable.Setting_itemTitle);
            subTitle = getContext().obtainStyledAttributes(attrs, R.styleable.Setting)
                    .getString(R.styleable.Setting_itemSubTitle);

            this.binding.title.setText(title);
            this.binding.subTitle.setText(subTitle);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
    }
}
