package com.cdef.orderappmanager.View.Adapter;

import android.arch.lifecycle.LifecycleOwner;
import android.support.annotation.LayoutRes;
import android.view.View;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.orderappmanager.AppManagerApplication;
import com.cdef.orderappmanager.DataModel.Login.AppInfo;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.View.ViewHolder.AppListAdapterViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.ObservableSnapshotArray;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.Query;

import java.util.Currency;
import java.util.Locale;


/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class AppListAdapter extends FirebaseRecyclerAdapter<AppInfo, AppListAdapterViewHolder> {
    String won = Currency.getInstance(Locale.KOREA).getSymbol();

    RequestManager rm;
    RequestOptions ro;
    public static AppListAdapter.OnItemClickListener listener;

    public void setListener(AppListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public AppListAdapter(ObservableSnapshotArray<AppInfo> dataSnapshots, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass, LifecycleOwner owner) {
        super(dataSnapshots, modelLayout, viewHolderClass, owner);
    }

    public AppListAdapter(ObservableSnapshotArray<AppInfo> dataSnapshots, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass) {
        super(dataSnapshots, modelLayout, viewHolderClass);
    }

    public AppListAdapter(SnapshotParser<AppInfo> parser, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass, Query query) {
        super(parser, modelLayout, viewHolderClass, query);
    }

    public AppListAdapter(SnapshotParser<AppInfo> parser, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(parser, modelLayout, viewHolderClass, query, owner);
    }

    public AppListAdapter(Class<AppInfo> modelClass, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass, Query query) {
        super(modelClass, modelLayout, viewHolderClass, query);

    }

    public AppListAdapter(Class<AppInfo> modelClass, @LayoutRes int modelLayout, Class<AppListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
        super(modelClass, modelLayout, viewHolderClass, query, owner);
    }

    public ObservableSnapshotArray<AppInfo> getData() {
        return this.mSnapshots;
    }

    public boolean findData(String key) {

        for (int index = 0; index < getItemCount(); index++) {
            AppInfo cur = getItem(index);
            if (cur != null) {
                if (cur.key.equals(key))
                    return true;
            }
        }
        return false;
    }

    public void setRequestManager(RequestManager requestManager) {
        this.rm = requestManager;
        this.ro = new RequestOptions();
        try {
            this.ro.transform(new RoundedCorners(Utils.dpToPx(AppManagerApplication.mContext, 10)));
        } catch (Exception e) {
        }
    }


    @Override
    protected void populateViewHolder(AppListAdapterViewHolder holder, AppInfo dataSet, int position) {
        LogUtil.d("리스트 구성중 : " + position);
        holder.binding.setApp(dataSet);
        holder.binding.setListener(listener);
        holder.binding.setInstalled(Utils.isInstalled(AppManagerApplication.mContext, dataSet.pkgName));
        if (this.rm != null) {
            this.rm.load(dataSet.iconImage).apply(this.ro).into(holder.binding.imageAppIcon);
        }


    }


    public void onDestroy() {
        this.won = null;
    }


    public interface OnItemClickListener {
        void onClickItem(AppInfo appInfo);
    }


}
