package com.cdef.orderappmanager.View.Activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cdef.orderappmanager.AppManagerApplication;

/**
 * Created by kimjinmo on 2018. 08. 23
 * <p>
 * 부모 액티비티
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManagerApplication.mContext = this;
    }

    View decorView;
    int uiOption;


    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();
            uiOption = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        decorView.setSystemUiVisibility(uiOption);
    }

}
