package com.cdef.orderappmanager.View.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.orderappmanager.AppManagerApplication;
import com.cdef.orderappmanager.BuildConfig;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.databinding.ActivityMain2Binding;
import com.cdef.orderappmanager.databinding.DialogMessageviewBinding;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.github.lzyzsd.circleprogress.ArcProgress;

import java.io.File;


/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class DownloaderDialog extends Dialog {

    ////멤버변수
    String fileName;

    int downloadId = 0;

    DialogMessageviewBinding binding;

    void buttonState(View view) {
        if (downloadId == 0) {
//            Toast.makeText(getApplicationContext(), "다운로드 받을 항목이 없습니다.", Toast.LENGTH_SHORT).show();
            dismiss();
        } else {

            switch (PRDownloader.getStatus(downloadId)) {
                case PAUSED:
                    Toast.makeText(AppManagerApplication.mContext, "다운로드를 재개합니다. 잠시만 기다려주세요.", Toast.LENGTH_SHORT).show();
                    PRDownloader.resume(downloadId);
                    break;
                case COMPLETED:
//                    Toast.makeText(AppManagerApplication.mContext, "다운로드 완료", Toast.LENGTH_SHORT).show();
                    ///실행하는 코드가 필요함.
                    break;
                case QUEUED:
                    break;
                case RUNNING:
                    PRDownloader.pause(downloadId);
                    break;
                case UNKNOWN:
                    break;
                case CANCELLED:
                    PRDownloader.resume(downloadId);
                    break;
            }

        }

    }

    String url;
    private Context mContext;
    private static DownloaderDialog mInstance;

    private String downloadCompletedFileName = null;
    ////

    public static DownloaderDialog getInstance(Context context) {
        if (DownloaderDialog.mInstance != null) {
            DownloaderDialog.mInstance.dismiss();
        }

        if (DownloaderDialog.mInstance == null) {
            DownloaderDialog.mInstance = new DownloaderDialog(context);
        }

        DownloaderDialog.mInstance.init(context);
        return DownloaderDialog.mInstance;
    }

    public static void clearInstance() {
        DownloaderDialog.mInstance = null;
    }


    private DownloaderDialog(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    private DownloaderDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private DownloaderDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_messageview, null, false);
        this.binding.setDialog(this);
        setContentView(binding.getRoot());

        setCanceledOnTouchOutside(false);
        ///1. 윈도우 셋팅
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        int calcWidth = (int) (displayMetrics.widthPixels * 0.3);


        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.binding.layoutMain.getLayoutParams();
        lp.width = (calcWidth > Utils.dpToPx(mContext, 320) ? calcWidth : Utils.dpToPx(mContext, 320));
        this.binding.layoutMain.setLayoutParams(lp);

        downloadCompletedFileName = null;
        this.binding.setState(0);
    }

    public DownloaderDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;

        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        ///다운로드 작업에 대한 해제

        PRDownloader.cancelAll();
        this.mContext = null;
        DownloaderDialog.mInstance = null;
        downloadId = 0;
    }


    public DownloaderDialog setURL(String url) {
        this.url = url;
        return this;
    }

    public DownloaderDialog setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void complete() {
        LogUtil.d("DownloaderDialog 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    @Override
    public void show() {
        super.show();
        ///보여진 이후 실행할 코드
        ///검사후 다운로드를 시작한다.
        LogUtil.d("다운로드 할 파일 정보 fileName: " + fileName);

        File alreadyExistFiles = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), fileName);
        LogUtil.d("다운로드 할 파일 정보 alreadyExistFiles: " + alreadyExistFiles);

        if (alreadyExistFiles != null && alreadyExistFiles.exists()) {

            AlertDialog dialog = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("알림")
                    .setMessage("같은 이름의 파일이 존재합니다. 새로 다운받으시겠습니까?")
                    .setPositiveButton("새로받기", (dialogInterface, i) -> {
                        downloadProccess();
                    })
                    .setNegativeButton("기존 파일 열기", (dialogInterface, i) -> {
                        downloadCompletedFileName = fileName;
                        goInstallation();
                    }).setCancelable(false)
                    .setOnCancelListener(dialogInterface -> {
                        dismiss();
                    })
            ;
            dialog = builder.create();
            dialog.show();
        } else

        {
            downloadProccess();
        }

    }

    //            <!--0: none, 1: donwloading, 2: complete, 3: error, 4: pause-->
    public void clickButtonState() {
        switch (this.binding.getState()) {
            case 0:
                downloadProccess();
                break;
            case 1:
                PRDownloader.pause(this.downloadId);
                break;
            case 2:
                goInstallation();
                break;
            case 3:
                downloadProccess();
                break;
            case 4:
                PRDownloader.resume(this.downloadId);
                break;
        }
    }

    private void downloadProccess() {


        final File dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        PRDownloader.cancel(this.downloadId);
        LogUtil.d("다운로드 주소 : " + url);
        this.downloadId = PRDownloader.download(url, dirPath.getAbsolutePath(), fileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        Log.d("kk9991", "onStartOrResume");
                        binding.setState(1);
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                        Log.d("kk9991", "onPause");
                        binding.setState(4);

                    }
                })
                .setOnCancelListener(new com.downloader.OnCancelListener() {
                    @Override
                    public void onCancel() {
                        Log.d("kk9991", "onCancel");
                        binding.setState(3);

                        File alreadyExistFiles = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), fileName);
                        if(alreadyExistFiles != null)
                            alreadyExistFiles.delete();


                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
                        if (binding != null)
                            binding.arcPprogress.setProgress((int) progressPercent);

                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("kk9991", "onDownloadComplete");
                        binding.setState(2);
                        downloadCompletedFileName = fileName;
                        goInstallation();
                    }

                    @Override
                    public void onError(Error error) {
                        Log.d("kk9991", "onError isConnectionError: " + error.isConnectionError());
                        Log.d("kk9991", "onError isServerError: " + error.isServerError());
                        binding.setState(3);
//                        if (error != null)   //에러 발생시 재시작
//                        {
//                            Toast.makeText(mContext, "오류가 발생해 다시 다운로드 합니다.", Toast.LENGTH_SHORT).show();
//                            downloadProccess();
//                        }
                    }
                });
    }

    public void goInstallation() {

        Log.d("kk9991", "onDownloadComplete");
        File toInstall = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), downloadCompletedFileName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            dismiss();
            Uri apkUri = FileProvider.getUriForFile(AppManagerApplication.mContext, BuildConfig.APPLICATION_ID + ".provider", toInstall);
            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            intent.setData(apkUri);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            AppManagerApplication.mContext.startActivity(intent);
        } else {
            dismiss();
            Uri apkUri = Uri.fromFile(toInstall);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            AppManagerApplication.mContext.startActivity(intent);
        }
    }
}
