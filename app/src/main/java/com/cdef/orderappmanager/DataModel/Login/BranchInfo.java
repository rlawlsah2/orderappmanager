package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class BranchInfo extends BaseData {
    @SerializedName("storeBranchUid")
    public String storeBranchUid;
    @SerializedName("branchName")
    public String branchName;
    @SerializedName("likes")
    public int likes;
    @SerializedName("isLiking")
    public boolean isLiking;
    @SerializedName("univInfo")
    public UnivInfo univInfo;
    @SerializedName("managerName")
    public String managerName;
    @SerializedName("branchTel")
    public String branchTel;
    @SerializedName("staffUid")
    public String staffUid;
    @SerializedName("posIp")
    public String posIp;
    @SerializedName("branchId")
    public String branchId;
    @SerializedName("tablets")
    public int tablets;
    @SerializedName("fbBrandName")
    public String fbBrandName;
    @SerializedName("storeOption")
    public StoreOption storeOption;
}
