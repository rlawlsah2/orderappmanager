package com.cdef.orderappmanager.DataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2017. 4. 28..
 */
@Generated("org.jsonschema2pojo")
public class OTPData {

    @SerializedName("maintenance")
    public boolean maintenance;
    @SerializedName("password")
    public String password;
    @SerializedName("expiresTo")
    public double expiresTo;

}

