package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2016. 11. 10..
 */
@Generated("org.jsonschema2pojo")
public class LoginData extends BaseData {

    @SerializedName("accessToken")
    public String accessToken;
    @SerializedName("branchInfo")
    public BranchInfo branchInfo;
    @SerializedName("apiKey")
    public String apiKey;
    @SerializedName("tableMap")
    public ArrayList<TableMap> tableMap;
}
