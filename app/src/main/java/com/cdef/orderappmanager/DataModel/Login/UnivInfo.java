package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class UnivInfo extends BaseData {
    @SerializedName("storeBranchUid")
    public String storeBranchUid;
    @SerializedName("fbBrandName")
    public String fbBrandName;
    @SerializedName("univName")
    public String univName;
    @SerializedName("univUid")
    public int univUid;
    @SerializedName("commission")
    public double commission;
    @SerializedName("logo")
    public String logo;
    @SerializedName("appLayout")
    public AppLayout appLayout;

}