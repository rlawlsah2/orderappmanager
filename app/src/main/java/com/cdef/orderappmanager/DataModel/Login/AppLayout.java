package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class AppLayout extends BaseData {
    @SerializedName("appManager")
    public AppManager appManager;

    public class AppManager extends BaseData {

        @SerializedName("background")
        public String background;
        @SerializedName("buttonArea")
        public String buttonArea;

    }

}