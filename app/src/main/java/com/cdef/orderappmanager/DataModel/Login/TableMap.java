package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class TableMap extends BaseData implements Serializable {
    @SerializedName("storeBranchUid")
    public String storeBranchUid;
    @SerializedName("tables")
    public int tables;
    @SerializedName("hallOrder")
    public int hallOrder;
    @SerializedName("hallName")
    public String hallName;


    @SerializedName("tableMapJson")
    public ArrayList<TableMapItem> talbeMapJson;

    public TableMap()
    {

    }

}
