package com.cdef.orderappmanager.DataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by kimjinmo on 2017. 4. 28..
 */
@Generated("org.jsonschema2pojo")
public class StatusData {

    @SerializedName("code")
    public String code;
    @SerializedName("message")
    public String message;

}
