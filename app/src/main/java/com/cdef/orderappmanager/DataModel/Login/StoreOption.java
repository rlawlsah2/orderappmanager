package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class StoreOption extends BaseData {
    @SerializedName("posUse")
    public boolean posUse;
    @SerializedName("advertisement")
    public boolean advertisement;
    @SerializedName("present")
    public boolean present;
    @SerializedName("tableChat")
    public boolean tableChat;
    @SerializedName("storeChat")
    public boolean storeChat;
    @SerializedName("kitchenTab")
    public boolean kitchenTab;
    @SerializedName("tableSetting")
    public boolean tableSetting;
    @SerializedName("posType")
    public String posType;
}
