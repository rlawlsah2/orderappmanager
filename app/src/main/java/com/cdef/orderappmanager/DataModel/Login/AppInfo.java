package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class AppInfo extends BaseData implements Serializable {

    public String key;  //firebase에서 키값

    @SerializedName("appName")
    public String appName;
    @SerializedName("pkgName")
    public String pkgName;
    @SerializedName("sourceType")
    public String sourceType;
    @SerializedName("iconImage")
    public String iconImage;
    @SerializedName("url")
    public String url;
    @SerializedName("targetVersionCode")
    public int targetVersionCode;
    @SerializedName("appType")
    public String appType;

    public AppInfo()
    {

    }

}
