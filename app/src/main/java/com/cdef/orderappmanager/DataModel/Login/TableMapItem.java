package com.cdef.orderappmanager.DataModel.Login;

import com.cdef.orderappmanager.DataModel.BaseData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class TableMapItem extends BaseData implements Serializable {
    @SerializedName("tNo")
    public String tNo;
    @SerializedName("x")
    public int x;
    @SerializedName("y")
    public int y;
    @SerializedName("width")
    public int width;
    @SerializedName("height")
    public int height;
    @SerializedName("tableId")
    public String tableId;

    public TableMapItem()
    {

    }

}
