package com.cdef.orderappmanager.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class UserNotFoundExceptions extends Exception {

    public UserNotFoundExceptions(String message)
    {
        super(message);
    }
}
