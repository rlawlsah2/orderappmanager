package com.cdef.orderappmanager.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class RestartExceptions extends Exception {

    public RestartExceptions(String message)
    {
        super(message);
    }
}
