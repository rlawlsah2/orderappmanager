package com.cdef.orderappmanager.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class NetworkExceptions extends Exception {

    public NetworkExceptions(String message)
    {
        super(message);
    }
}
