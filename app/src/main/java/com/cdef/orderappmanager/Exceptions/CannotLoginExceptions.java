package com.cdef.orderappmanager.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class CannotLoginExceptions extends Exception {

    public CannotLoginExceptions(String message)
    {
        super(message);
    }
}
