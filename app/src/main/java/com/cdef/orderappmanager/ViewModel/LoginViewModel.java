package com.cdef.orderappmanager.ViewModel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.cdef.orderappmanager.DataModel.Login.AppInfo;
import com.cdef.orderappmanager.DataModel.Login.BranchInfo;
import com.cdef.orderappmanager.DataModel.Login.LoginData;
import com.cdef.orderappmanager.DataModel.OTPData;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Networking.NetworkUtil;
import com.cdef.orderappmanager.Repository.LoginRepository;
import com.cdef.orderappmanager.Utils.TabletSettingUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginViewModel extends BaseViewModel {


    //    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private LoginRepository loginRepository;

    private MutableLiveData<ArrayList<AppInfo>> appList;

    public LoginViewModel() {
        this.appList = new MutableLiveData<>();

    }

    public void initRepository(Context context) {
        this.loginRepository = new LoginRepository(NetworkUtil.getInstance(context).getService());


    }

    public Observable<LoginData> login() {

        state.postValue(STATE.LOADING);
        return loginRepository.login()
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .flatMap(loginData -> {
                    TabletSettingUtils.getInstacne().setBranchInfo(loginData.branchInfo);
                    TabletSettingUtils.getInstacne().setApiKey(loginData.apiKey);

                    return Observable.just(loginData);

                });
    }


    public Observable<OTPData> getOTP(String url) {

        state.postValue(STATE.LOADING);
        return loginRepository.getOTP(url)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .flatMap(otpData -> {
                    return Observable.just(otpData);
                });
    }

    public DatabaseReference getFBRefrence() {
        try {
            return FirebaseDatabase.getInstance("https://orderappmanager.firebaseio.com/").getReference()
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName)
                    .child("branches")
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId);
        } catch (Exception e) {
            return null;
        }
    }

    public Observable<ArrayList<AppInfo>> getAppList() {
        getFBRefrence()
                .child("apps")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        LogUtil.d("새로운 디비 스키마 보자 : " + dataSnapshot);
                        ArrayList<AppInfo> list = new ArrayList<>();
                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            list.add(item.getValue(AppInfo.class));
                        }

                        appList.setValue(list);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        LogUtil.d("새로운 디비 스키마 보자니까 왜 실패함 : " + databaseError);

                    }
                });
        return null;

    }


    /**
     * fail 대한 처리를 위임하기 위한 코드
     */
    public interface ThrowableListener {
        void throwable(HashMap<String, Object> messages);
    }

    /**
     * success 대한 처리를 위임하기 위한 코드
     */
    public interface SuccessListener {
        void success(HashMap<String, Object> messages);
    }
}
