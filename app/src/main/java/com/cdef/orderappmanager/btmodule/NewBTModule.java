package com.cdef.orderappmanager.btmodule;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import com.cdef.orderappmanager.LogUtil;
import com.chipsen.bleservice.BluetoothLeService;
import com.chipsen.bleservice.SampleGattAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 1. 2..
 *
 * [핵심 코드]
 * 블루투스 service 등록.
 * 블루투스 조명과의 연결 상태를 알려줄 리시버 등록
 *

 */

public class NewBTModule {
    private static final long SCAN_PERIOD = 10 * 1000;  // 스캔 시간. 10초
    private boolean isOnlyScanning = false;

    ///필요한 객체들.
    Context mContext;
    private static NewBTModule instance;   // 싱글턴을 위한 static 객체
    BluetoothGattCharacteristic PWM_Read_Write;
    BluetoothLeScanner mBLEScanner; //BT LE 장치 스캔을 위한 객체.
    private boolean mIsScanning;    //스캔중인지 나타냄.
    private boolean mIsConnected;   //연결 여부
    private BluetoothLeService mBluetoothLeService; //칩센에서 제공하는 블루투스모듈 서비스 객체.
    private CompositeSubscription subscription;
    private String mTableNo = "0";
    private static String lastHex;
    final Handler handler = new Handler();  //연결뒤 일정 딜레이후에 실행하기 위한 핸들러.
    BTModuleInterface btModuleInterface_servie, btModuleInterface_activity;
    String[] off = {String.valueOf(0),String.valueOf(0),String.valueOf(0),String.valueOf(0)};
    /**
     * 연결할 BT모듈의 이름을 지정
     * **/
    public NewBTModule setmTableNo(String tableNo)
    {
        this.mTableNo = tableNo;
        return this;
    }

    /**
     * BT모듈의 이름을 날려버리고 동시에 모든 상황을 초기화 해버린다.
     * **/
    public void clearTableNo()
    {

    }

    public NewBTModule setBtModuleInterface_service(BTModuleInterface btModuleInterface) {
        this.btModuleInterface_servie = btModuleInterface;
        return this;
    }
    public NewBTModule setBtModuleInterface_activity(BTModuleInterface btModuleInterface) {
        this.btModuleInterface_activity = btModuleInterface;
        return this;
    }

    public static NewBTModule getInstance(Context context)
    {
        if(NewBTModule.instance == null)
        {
            NewBTModule.instance = new NewBTModule(context);
        }


        return NewBTModule.instance;
    }

    ///객체 초기화
    private NewBTModule(Context context)
    {
        ///초기화
        this.mContext = context;
        this.mBLEScanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
        subscription = new CompositeSubscription();

        bindService();

    }

    private void bindService()
    {
        // Start service
        //블루투스 서비스가 시작된다.(폰에서 자동으로 블루투스를 켜고 시작한다.)
        //없으면 앱이 실행되지 않는다.
        Intent gattServiceIntent = new Intent(this.mContext, BluetoothLeService.class);
        (mContext).bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    //블루투스 모듈에 연결하기위한 service 객체. 단, onServiceConnected안에 있는 service로 부터 mBluetoothLeService를 가져와 제어한다.
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            LogUtil.d("Navig_Service Connected");

            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                LogUtil.d("Navig_Service Disconnected");
            }
            // Automatically connects to the device upon successful start-up initialization.

            ///나중에 이쪽 코드 수정해야함.
//            LogUtil.d("자 접속하려는데 뭐가 문제야 : mBluetoothLeService" + mBluetoothLeService + " 와 " + mPairDeviceName);
//            if(mPairDeviceName != null && mPairDeviceAddress != null)
//                mBluetoothLeService.connect(mPairDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
            LogUtil.d("Navig_Service Disconnected");

        }
    };

    public void connect(String address)
    {
        if(mBluetoothLeService != null)
        {

            try {
                mBluetoothLeService.connect(address);
            }
            catch (Exception e)
            {
                mBluetoothLeService = null;
                bindService();
            }
        }
    }


    /**
     * broadcastReceiver 등록시 사용할 필터
     * **/
    private IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        return intentFilter;
    }

    /**
     * 블루투스와 통신을 위한 receiver를 등록/해제
     * **/
    public void registerReceiver()
    {
       try{ mContext.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter()); } catch (Exception e) { }
    }
    public void unregisterReceiver()
    {
        try { mContext.unregisterReceiver(mGattUpdateReceiver);} catch (Exception e) {}

    }

    /**
     * 블루투스 모듈의 상태확인 receiver
     * **/
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LogUtil.d("Dbg_Navig_mGattUpdateReceiver: " + action);

            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

                LogUtil.d("조명 연결 되었음.");
                mIsConnected = true;
                if(mContext != null && mContext instanceof Activity)
                    Toast.makeText(mContext, "기기에 연결되었습니다.", Toast.LENGTH_SHORT).show();

                if(btModuleInterface_servie != null)
                    btModuleInterface_servie.onConnect();
                if(btModuleInterface_activity != null)
                    btModuleInterface_activity.onConnect();

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                LogUtil.d("조명 연결 끊어짐.");

                mIsConnected = false;
                scanStop();
                //초기화를 안해주면 재접속시 문제 발생.
                if(mBluetoothLeService != null) {
                    mBluetoothLeService.disconnect();
                    mBluetoothLeService.initialize();
                }
                //리커버리 코드 추가
//                scanLeDeviceRecovery();

                if(btModuleInterface_servie != null)
                    btModuleInterface_servie.onDisconnect();
                if(btModuleInterface_activity != null)
                    btModuleInterface_activity.onDisconnect();
            }
            else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                findCharacteristic();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setColor("#ffffff");
                    }
                }, 3500);
                LogUtil.d("기기연결");
            }
        }
    };


    /**
     *  블루투스 통신이 끊어졌을때 호출되는 메소드
     *
     * **/
    private void scanLeDeviceRecovery() {
//        devicesCount=0;
//        this.mFoundDevicesList.clear();
        // Stops scanning after a pre-defined scan period.
        if(!mIsScanning)
        {

            if(this.mTableNo == null || this.mTableNo.equals("0"))
            {
                ///이경우엔 돌면 안되지
                Toast.makeText(mContext, "장치를 찾고 다시 시도해주세요.", Toast.LENGTH_SHORT).show();

            }
            else
            {
                mIsScanning = true;
                mBLEScanner.startScan(mScanCallback);
                if(this.mContext != null && this.mContext instanceof Activity)
                    Toast.makeText(mContext, "기기를 찾고있습니다.", Toast.LENGTH_SHORT).show();
            }

        }
    }



    /**
     * 스캔시 필요한 동작이 포함된 callback (기본)
     * *Recovery 로직상 mPairDeviceName 부분에 이전에 접속했던 항목이 저장되어있는데, 재 검색시 mPairDeviceName와 일치하는 device를 찾으면 바로 연결시도&스캔중단

     * **/
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            LogUtil.d("스캔실패");
        }

        private void processResult(final ScanResult result) {
            LogUtil.d("스캔중 : " + result);
            if(result != null && result.getDevice() != null && result.getDevice().getName() != null && result.getDevice().getName().equals(mTableNo))
            {

                ///처음 돌때
//                    mPairDeviceName = result.getDevice().getName();
//                    mPairDeviceAddress = result.getDevice().getAddress();
//                    scanStop();
//                    if(mBluetoothLeService != null)
//                        mBluetoothLeService.connect(mPairDeviceAddress);


            }
        }
//            });
//        }
    };


    /**
     * 스캔시 필요한 동작이 포함된 callback (스캔용)
     * **/
    private ScanCallback mOnlyScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            LogUtil.d("스캔실패");
        }

        private void processResult(final ScanResult result) {
            LogUtil.d("스캔중 : " + result);

            if(btModuleInterface_activity != null && result != null)
                btModuleInterface_activity.onScanning(result);

        }
    };

    public void disconnect()
    {
        mIsConnected = false;
        scanStop();
        //초기화를 안해주면 재접속시 문제 발생.
        if(mBluetoothLeService != null) {
            mBluetoothLeService.disconnect();
            mBluetoothLeService.initialize();
        }
    }

    /// 리시버를 움직이게 할것.
    public void wakeReceiver()
    {
        Intent intent = new Intent(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        mContext.sendBroadcast(intent);
        LogUtil.d("조명 찾기 시작");
    }

    ///다른 블루투스 장치 검색을 중단한다. scanner 객체와 callback을 이용해 중지.
    private void scanStop()
    {
//        mBluetoothLeService.connect(mPairDeviceAddress);
        mIsScanning = false;
        if(mBLEScanner != null)
            mBLEScanner.stopScan(mScanCallback);
    }

    public void onlyScanStop()
    {
        this.isOnlyScanning = false;
        if(mBLEScanner != null)
            mBLEScanner.stopScan(mOnlyScanCallback);
        if(this.btModuleInterface_activity != null)
            this.btModuleInterface_activity.onScanComplete();
    }

    public void onlyScanStart()
    {
        if(mBLEScanner != null)
        {
            this.isOnlyScanning = true;

            ////스캔한지 SCAN_PERIOD 만큼의 시간후 중단하기.
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    onlyScanStop();
                }
            }, SCAN_PERIOD);
            mBLEScanner.startScan(mOnlyScanCallback);
        }

    }

    public boolean istScanning()
    {
        return isOnlyScanning;
    }



    public void findCharacteristic() {
        // Find BLE112 service for writing to
        List<BluetoothGattService> gattServices = mBluetoothLeService.getSupportedGattServices();

        if (gattServices == null) return;
        String uuid = null;

        // Loops through available GATT Services.
        //������ GATT ���񽺸� ���� UUID�� ã�´�.
        for (BluetoothGattService gattService : gattServices)
        {
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics)
            {
                uuid = gattCharacteristic.getUuid().toString();
                if (SampleGattAttributes.PWM_READ_WRITE_UUID.equals(uuid))	PWM_Read_Write = gattCharacteristic;
            }
        }
    }



    ///////////////////////////////연결된 이후에 대한 코드

    public void setColor(String hex)
    {
        hex = hex.replace("#", "");
        Random random = new Random();
        int a = random.nextInt(255);
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        NewBTModule.lastHex = hex;
        setRGB(rgb);
    }



    // PWM 출력
    void setRGB(String[] rgb) {
        LogUtil.d("불빛 변경 : " + rgb);
        byte[] PWM_Write_byte = new byte[4];
        PWM_Write_byte[0]= (byte) (Integer.parseInt(rgb[3]) >>> 0 & 0xff);
        PWM_Write_byte[1]= (byte) (Integer.parseInt(rgb[2]) >>> 0 & 0xff);
        PWM_Write_byte[2]= (byte) (Integer.parseInt(rgb[1]) >>> 0 & 0xff);
        PWM_Write_byte[3]= (byte) (Integer.parseInt(rgb[0]) >>> 0 & 0xff);

        try {
            mBluetoothLeService.writeCharacteristic(PWM_Read_Write, PWM_Write_byte);
        }
        catch (NullPointerException e)
        {
            LogUtil.d("블루투스 장치 연결 안됨.");
            if(e != null)
            {
                try {
                    mBluetoothLeService = ((BluetoothLeService.LocalBinder) mServiceConnection).getService();
                }
                catch (ClassCastException e1)
                {

                }

            }
        }
    }

    public void randomColor()
    {
        Random random = new Random();
        int a = random.nextInt(255);
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        setRGB(rgb);
    }

    public interface BTModuleInterface{
        void onConnect();
        void onDisconnect();
        void onScanning(ScanResult result);
        void onScanComplete();
    }

    public boolean isPaired()
    {
//        LogUtil.d("블루투스 모듈 isPaired : " + BluetoothAdapter.getDefaultAdapter().getBondedDevices());
//        for(BluetoothDevice device : BluetoothAdapter.getDefaultAdapter().getBondedDevices())
//        {
//            LogUtil.d("과거 연결됐었던 기기 정보 : " + device.getName());
//        }
//
//        if(BluetoothAdapter.getDefaultAdapter().getBondedDevices() == null || BluetoothAdapter.getDefaultAdapter().getBondedDevices().size() == 0 )
//        {
//            LogUtil.d("블루투스 모듈 isPaired false");
//            return false;
//        }
//        else
//        {
//            LogUtil.d("블루투스 모듈 mIsConnected : " + mIsConnected);
//            return true;
//        }
        return mIsConnected;
    }



    /**
     * 켜는시간 0.1초 끄는시간 0.1초
     * **/
    public void flickering(int count)
    {
        Random random = new Random();


        ArrayList<String[]> arrayList = new ArrayList<>();
        for(int i=0; i < count; i++)
        {
            int a = random.nextInt(255);
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);
            String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};

            arrayList.add(rgb);
            arrayList.add(off);
        }
        if(NewBTModule.lastHex != null)
            arrayList.add(getColorHex(NewBTModule.lastHex));
        else
        {
            if( count > 0)
            {
                int a = random.nextInt(255);
                int r = random.nextInt(255);
                int g = random.nextInt(255);
                int b = random.nextInt(255);
                String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
                arrayList.add(rgb);
            }
        }

        Subscription ob = Observable.zip(
                Observable.interval(100, TimeUnit.MILLISECONDS).take(arrayList.size()),
                Observable.from(arrayList),
                new Func2<Long, String[], Object>() {
                    @Override
                    public Object call(Long aLong, String[] strings) {
                        setRGB(strings);
                        return null;
                    }
                })
                .subscribe(
                );
        this.subscription.add(ob);

    }

    public String[] getColorHex(String hex)
    {
        hex = hex.replace("#", "");
        Random random = new Random();
        int a = random.nextInt(255);
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        return rgb;
    }
}
