package com.cdef.orderappmanager.btmodule;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import com.cdef.orderappmanager.LogUtil;
import com.chipsen.bleservice.BluetoothLeService;
import com.chipsen.bleservice.SampleGattAttributes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Func2;
import rx.subscriptions.CompositeSubscription;


/**
 * Created by kimjinmo on 2017. 9. 5..
 */

public class BTModule {



    private static String lastHex;
    private String mTableNo = "0";

    private static final long SCAN_PERIOD = 10000;  // 스캔 시간. 10초
    BluetoothGattCharacteristic PWM_Read_Write; //
    BluetoothAdapter mBTAdapter;    //블루투스 모듈에 접근할 수 있는 객체
    private static BTModule instance;   // 싱글턴을 위한 static 객체
    boolean Search_state = true;    //현재 상태
    int devicesCount = 0;       //찾은 기기 수
    private Handler mHandler;   //scan을 스레드로 돌리기 위함
    private boolean mIsScanning; //스캔중
    private boolean mIsConnected;   //연결중
    private Context mContext;
    private CompositeSubscription subscription;
    BluetoothLeScanner mBLEScanner;

    HashMap<String, BluetoothDevice> mFoundDevicesList;
    private String mPairDeviceName;
    private String mPairDeviceAddress;

    final Handler handler = new Handler();


    private BTModule(Context context, String tableNo)
    {
        this.mFoundDevicesList = new HashMap<>();
        this.mBTAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mHandler = new Handler();
        this.mBLEScanner = this.mBTAdapter.getBluetoothLeScanner();
        this.mContext = context;
        this.mTableNo = tableNo;
        subscription = new CompositeSubscription();


        // Start service
        //블루투스 서비스가 시작된다.(폰에서 자동으로 블루투스를 켜고 시작한다.)
        //없으면 앱이 실행되지 않는다.
        Intent gattServiceIntent = new Intent(this.mContext, BluetoothLeService.class);
        (mContext).bindService(gattServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public static BTModule getInstance(Context context, String tableNo)
    {
        if(BTModule.instance == null)
        {
            BTModule.instance = new BTModule(context,tableNo);
        }


        return BTModule.instance;
    }
    private BluetoothLeService mBluetoothLeService;


    /**
     * 블루투스 모듈의 상태확인 receiver
     * **/
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            LogUtil.d("Dbg_Navig_mGattUpdateReceiver: " + action);

            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

                LogUtil.d("조명 연결 되었음.");

                mIsConnected = true;

                if(mContext != null && mContext instanceof Activity)
                    Toast.makeText(mContext, "기기에 연결되었습니다.", Toast.LENGTH_SHORT).show();

            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mIsConnected = false;
                scanStop();
                //초기화를 안해주면 재접속시 문제 발생.
                if(mBluetoothLeService != null) {
                    mBluetoothLeService.disconnect();
                    mBluetoothLeService.initialize();
                }
//                ((Activity)mContext).off();
                //리커버리 코드 추가
                scanLeDeviceRecovery();

            }
            else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {

                findCharacteristic();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setColor("#ffffff");
                    }
                }, 3500);
                LogUtil.d("기기연결");
//                ((Activity)mContext).on();


                // Show all the supported services and characteristics on the user interface.
                //서비스를 제공하고 있는지를 보여준다.
//                discoveredGattServices(mBluetoothLeService.getSupportedGattServices());
//                discoveredGattServices2(mBluetoothLeService.getSupportedGattServices());
            }

        }
    };
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            LogUtil.d("Navig_Service Connected");

            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
//                finish();
                LogUtil.d("Navig_Service Disconnected");

            }
            // Automatically connects to the device upon successful start-up initialization.
            LogUtil.d("자 접속하려는데 뭐가 문제야 : mBluetoothLeService" + mBluetoothLeService + " 와 " + mPairDeviceName);
            if(mPairDeviceName != null && mPairDeviceAddress != null)
                mBluetoothLeService.connect(mPairDeviceAddress);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
            LogUtil.d("Navig_Service Disconnected");

        }
    };


    /**
     * 블루투스와 통신을 위한 receiver를 등록/해제
     * **/
    public void registerReceiver()
    {
        mContext.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
    }
    public void unregisterReceiver()
    {
        mContext.unregisterReceiver(mGattUpdateReceiver);

    }

    public void wakeReciver()
    {
        Intent intent = new Intent(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        mContext.sendBroadcast(intent);
        LogUtil.d("조명 찾기 시작");

    }

    /**
     *  start scan
     */
    public void startScan()
    {
//        this.scanLeDevice(true);
        scanLeDeviceRecovery();

    }


    /**
     * @param enable : 스캔 할거냐 말꺼냐
     * **/
    private void scanLeDevice(final boolean enable) {
        devicesCount=0;
        this.mFoundDevicesList.clear();
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scanStop();
                    getFoundedDevicesList();
                }
            }, SCAN_PERIOD);
            mIsScanning = true;
            mBLEScanner.startScan(mScanCallback);

        } else {
            Search_state=true;
            mIsScanning = false;
            mBLEScanner.stopScan(mScanCallback);
        }
    }

    /**
     *  블루투스 통신이 끊어졌을때 호출되는 메소드
     *
     * **/
    private void scanLeDeviceRecovery() {
        devicesCount=0;
        this.mFoundDevicesList.clear();
        // Stops scanning after a pre-defined scan period.
        if(!mIsScanning)
        {
            mIsScanning = true;
            mBLEScanner.startScan(mScanCallback);
            if(this.mContext != null && this.mContext instanceof Activity)
                Toast.makeText(mContext, "기기를 찾고있습니다.", Toast.LENGTH_SHORT).show();

        }
    }

    private void getFoundedDevicesList()
    {
        for(String key : this.mFoundDevicesList.keySet())
        {
            LogUtil.d("등록된 장치 : " + key + " /// " + this.mFoundDevicesList.get(key));
        }

        final BluetoothDevice[] list = this.mFoundDevicesList.values().toArray(new BluetoothDevice[this.mFoundDevicesList.keySet().size()]);

        ArrayList<CharSequence> listTitle = new ArrayList<>();
        for(BluetoothDevice item : list)
        {
            listTitle.add(item.getName());
        }

//        this.mSelectorBuilder.setItems(listTitle.toArray(new CharSequence[listTitle.size()]), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                LogUtil.d("선택된 아이템 [" + i + "]  " + list[i].getName());
//
//                mPairDeviceName = list[i].getName();
//                mPairDeviceAddress = list[i].getAddress();
//
//                if(mBluetoothLeService != null)
//                    mBluetoothLeService.connect(mPairDeviceAddress);
//
//
//            }
//        });
//        this.mSelectorBuilder.show();
    }

    public void setPair(BluetoothDevice device)
    {

    }


    /**
     * 스캔시 필요한 동작이 포함된 callback
     *
     * *Recovery 로직상 mPairDeviceName 부분에 이전에 접속했던 항목이 저장되어있는데, 재 검색시 mPairDeviceName와 일치하는 device를 찾으면 바로 연결시도&스캔중단

     * **/
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            processResult(result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult result : results) {
                processResult(result);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            LogUtil.d("스캔실패");
        }

        private void processResult(final ScanResult result) {
//            ((Activity)mContext).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {

            //아까 썼던 거라면 recov.
//                    if(mPairDeviceName != null)
//                    {
//                        scanStop();
//                        if(mBluetoothLeService != null)
//                            mBluetoothLeService.connect(mPairDeviceAddress);
//                    }

            LogUtil.d("스캔중 : " + result);
            if(result != null && result.getDevice() != null && result.getDevice().getName() != null && result.getDevice().getName().equals(mTableNo))
            {


                //아까 썼던 거라면 recov.
                LogUtil.d("스캔중1 mPairDeviceName : " + mPairDeviceName);

                if(mPairDeviceName != null && result.getDevice().getName().equals(mPairDeviceName))
                {
                    LogUtil.d("스캔중2 mPairDeviceName : " + mPairDeviceName);

                    scanStop();
                    if(mBluetoothLeService != null)
                        mBluetoothLeService.connect(mPairDeviceAddress);
                }
                else
                {
                    LogUtil.d("스캔중3 mPairDeviceName : " + mPairDeviceName);

                    ///처음 돌때
                    mPairDeviceName = result.getDevice().getName();
                    mPairDeviceAddress = result.getDevice().getAddress();
                    scanStop();
                    if(mBluetoothLeService != null)
                        mBluetoothLeService.connect(mPairDeviceAddress);
                }

//
//                        if(mFoundDevicesList.get(result.getDevice().getAddress()) == null)
//                        {
//                            mFoundDevicesList.put(result.getDevice().getAddress(), result.getDevice());
//                        }


            }
        }
//            });
//        }
    };

    private void scanStop()
    {
//        mBluetoothLeService.connect(mPairDeviceAddress);
        mIsScanning = false;
        if(mBLEScanner != null)
            mBLEScanner.stopScan(mScanCallback);
        Search_state=true;
    }


    // 스캔시 필요한 callback 객체
//    private BluetoothAdapter.LeScanCallback mLeScanCallback =
//            new BluetoothAdapter.LeScanCallback() {
//
//                @Override
//                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
//                    if(mContext instanceof Activity)
//                    ((Activity)mContext).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            parseAdvertisementPacket(scanRecord);
//                        }
//
//                        void parseAdvertisementPacket(final byte[] scanRecord) {//Advertising ������
//                            byte[] advertisedData = Arrays.copyOfRange(scanRecord,0, scanRecord.length);
//                            final StringBuilder stringBuilder = new StringBuilder(advertisedData.length);
//                            for(byte byteChar : advertisedData){
//                                stringBuilder.append((char)  byteChar);
//                            }
//
//                            byte[] inputBytes = stringBuilder.toString().getBytes();
//                            String hexString = "";
//
//                            for (byte b : inputBytes) {
//                                hexString += Integer.toString((b & 0xF0) >> 4, 16);
//                                hexString += Integer.toString(b & 0x0F, 16);
//                            }
//
//                            LogUtil.d("검색 결과 : " + stringBuilder.toString());
//
//                        }
//                    });
//
//
//                }
//            };


    public void randomColor()
    {
        Random random = new Random();
        int a = random.nextInt(255);
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        setRGB(rgb);
    }

    public void setColor(String hex)
    {
        hex = hex.replace("#", "");
        Random random = new Random();
        int a = random.nextInt(255);
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        BTModule.lastHex = hex;
        setRGB(rgb);
    }

    public String[] getColorHex(String hex)
    {
        hex = hex.replace("#", "");
        Random random = new Random();
        int a = random.nextInt(255);
        int r = Integer.valueOf(hex.substring(0, 2), 16);
        int g = Integer.valueOf(hex.substring(2, 4), 16);
        int b = Integer.valueOf(hex.substring(4, 6), 16);
        String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
        LogUtil.d("입력된 r : " + r);
        LogUtil.d("입력된 g : " + g);
        LogUtil.d("입력된 b : " + b);
        return rgb;
    }
    public void setOFF()
    {
        setColor("#000000");
    }
    public void setON()
    {
        randomColor();
    }


    /**
     * 켜는시간 0.15초 끄는시간 0.15초
     * **/
    public void flickering(int count)
    {
        Random random = new Random();String[] off = {String.valueOf(0),String.valueOf(0),String.valueOf(0),String.valueOf(0)};

        ArrayList<String[]> arrayList = new ArrayList<>();
        for(int i=0; i < count; i++)
        {
            int a = random.nextInt(255);
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);
            String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};

            arrayList.add(rgb);
            arrayList.add(off);
        }
        if(BTModule.lastHex != null)
            arrayList.add(getColorHex(BTModule.lastHex));
        else
        {
            if( count > 0)
            {
                int a = random.nextInt(255);
                int r = random.nextInt(255);
                int g = random.nextInt(255);
                int b = random.nextInt(255);
                String[] rgb = {String.valueOf(r),String.valueOf(g),String.valueOf(b),String.valueOf(a)};
                arrayList.add(rgb);
            }
        }

        Subscription ob = Observable.zip(
                Observable.interval(150, TimeUnit.MILLISECONDS).take(arrayList.size()),
                Observable.from(arrayList),
                new Func2<Long, String[], Object>() {
                    @Override
                    public Object call(Long aLong, String[] strings) {
                        setRGB(strings);
                        return null;
                    }
                })
                .subscribe(
                );
        this.subscription.add(ob);

    }
    public void flickering(String hex, int count)
    {
        Random random = new Random();
        int a = random.nextInt(255);
        int r = random.nextInt(255);
        int g = random.nextInt(255);
        int b = random.nextInt(255);
//        String[] rgb = getColorHex(hex);
        String[] off = {String.valueOf(0),String.valueOf(0),String.valueOf(0),String.valueOf(0)};

        ArrayList<String[]> arrayList = new ArrayList<>();
        for(int i=0; i < count; i++)
        {
            arrayList.add(getColorHex(hex));
            arrayList.add(off);
        }

        if(BTModule.lastHex != null)
            arrayList.add(getColorHex(BTModule.lastHex));
        else
        {
            if( count > 0)
                arrayList.add(getColorHex(hex));
        }

        Subscription ob = Observable.zip(
                Observable.interval(150, TimeUnit.MILLISECONDS).take(arrayList.size()),
                Observable.from(arrayList),
                new Func2<Long, String[], Object>() {
                    @Override
                    public Object call(Long aLong, String[] strings) {
                        setRGB(strings);
                        return null;
                    }
                })
                .subscribe(
                );
        this.subscription.add(ob);

    }



    // PWM 출력
    void setRGB(String[] rgb) {


        LogUtil.d("불빛 변경 : " + rgb);
        byte[] PWM_Write_byte = new byte[4];
        PWM_Write_byte[0]= (byte) (Integer.parseInt(rgb[3]) >>> 0 & 0xff);
        PWM_Write_byte[1]= (byte) (Integer.parseInt(rgb[2]) >>> 0 & 0xff);
        PWM_Write_byte[2]= (byte) (Integer.parseInt(rgb[1]) >>> 0 & 0xff);
        PWM_Write_byte[3]= (byte) (Integer.parseInt(rgb[0]) >>> 0 & 0xff);

//        setPWM_Write(PWM_Write_byte);
        try {
            mBluetoothLeService.writeCharacteristic(PWM_Read_Write, PWM_Write_byte);
        }
        catch (NullPointerException e)
        {
//            ((Activity)mContext).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(mContext, "connection off", Toast.LENGTH_SHORT).show();
//                }
//            });
            LogUtil.d("블루투스 장치 연결 안됨.");
            if(e != null)
            {
                mBluetoothLeService = ((BluetoothLeService.LocalBinder) mServiceConnection).getService();

            }
        }

//        try {
//            Thread.sleep(50);
//        } catch (Exception ex) {
//        }

    }


    /**
     * broadcastReceiver 등록시 사용할 필터
     * **/
    private IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        return intentFilter;
    }


    public void findCharacteristic() {
        // Find BLE112 service for writing to
        List<BluetoothGattService> gattServices = mBluetoothLeService.getSupportedGattServices();

        if (gattServices == null) return;
        String uuid = null;

        // Loops through available GATT Services.
        //������ GATT ���񽺸� ���� UUID�� ã�´�.
        for (BluetoothGattService gattService : gattServices)
        {
            List<BluetoothGattCharacteristic> gattCharacteristics = gattService.getCharacteristics();
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics)
            {
                uuid = gattCharacteristic.getUuid().toString();
                if (SampleGattAttributes.PWM_READ_WRITE_UUID.equals(uuid))	PWM_Read_Write = gattCharacteristic;
            }
        }
    }



}


