package com.cdef.orderappmanager.btmodule;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Utils.PreferenceUtils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by kimjinmo on 2018. 1. 16..
 */

public class AdvancedNewBTModuleService extends Service {


    /**
     * 조명 명령에 대한 처리 리시버
     * intent에 다음 정보를 포함
     * type {normal,filkering,dim}
     **/
    private BroadcastReceiver LightReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent input) {
            LogUtil.d("조명 리시버 여기가 동작한다 : " + NewBTModule.getInstance(getApplicationContext()).isPaired());


            String type = input.getStringExtra("type");
            if (type != null) {
                Toast.makeText(getApplicationContext(), "조명 서비스 확인 : " + type, Toast.LENGTH_SHORT).show();


                switch (type) {
                    case "normal":
                        NewBTModule.getInstance(getApplicationContext()).randomColor();
                        break;
                    case "flickering":
                        NewBTModule.getInstance(getApplicationContext()).flickering(20);
                        break;
                    case "dim":
                        break;
                }
            } else {
            }


        }
    };


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NewBTModule.getInstance(this).disconnect();
        NewBTModule.getInstance(this).unregisterReceiver();
        unregisterReceiver(LightReceiver);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        LogUtil.d("조명 서비스 시작");
        checkBLUETOOTH();
        ////조명 색상 처리할 리시버
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.cdef.orderappmanager.LIGHT");
        registerReceiver(LightReceiver, intentFilter);

        NewBTModule.getInstance(this).setmTableNo("DreamEn").setBtModuleInterface_service(new NewBTModule.BTModuleInterface() {
            @Override
            public void onConnect() {
                checkBLUETOOTH();

            }

            @Override
            public void onDisconnect() {
                checkBLUETOOTH();
            }

            @Override
            public void onScanning(ScanResult result) {
            }

            @Override
            public void onScanComplete() {

            }
        }).registerReceiver();


        ///조명상태 체크
        Observable.interval(1, 3, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        aLong -> {
                            LogUtil.d("조명 상태 체크 로직 : (" + aLong + ")  : " + !NewBTModule.getInstance(getApplicationContext()).isPaired());
                            // here is the task that should repeat
                            // 조명 체크 로직
                            //1. 조명이 연결중인가 isPaired()
                            //2. false인 경우 최근접속기록 recentConnected() 확인
                            //3. true인 경우 연결 시도
                            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                                if (!NewBTModule.getInstance(getApplicationContext()).isPaired()) {
                                    if (isRecentConntected()) {
                                        String tmpName = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_NAME);
                                        String tmpAddress = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_ADDRESS);
                                        LogUtil.d("자동연결 : tmpName" + tmpName);
                                        LogUtil.d("자동연결 : tmpAddress" + tmpAddress);

                                        if (tmpName != null && tmpAddress != null) {
                                            LogUtil.d("자동연결");
                                            NewBTModule.getInstance(getApplicationContext()).connect(tmpAddress);
                                        }
                                    }
                                }

                            }
                        },
                        throwable -> {

                        },
                        () -> {

                        }

                );


    }

    private void checkBLUETOOTH() {

        if (BluetoothAdapter.getDefaultAdapter() == null) {
            //블루투스가 없음.
        } else {
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                //ON이네.
                LogUtil.d("AdvancedNewBTModuleService   checkBLUETOOTH : isPaired" + !NewBTModule.getInstance(this).isPaired());

                if (!NewBTModule.getInstance(this).isPaired()) {
                    LogUtil.d("AdvancedNewBTModuleService   checkBLUETOOTH : isRecentConntected" + isRecentConntected());

                    if (this.isRecentConntected()) {
                        if (!NewBTModule.getInstance(this).isPaired()) {
                            String tmpName = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_NAME);
                            String tmpAddress = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_ADDRESS);
                            LogUtil.d("자동연결 : tmpName" + tmpName);
                            LogUtil.d("자동연결 : tmpAddress" + tmpAddress);

                            if (tmpName != null && tmpAddress != null) {
                                LogUtil.d("자동연결");
                                NewBTModule.getInstance(this).connect(tmpAddress);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isRecentConntected() {
        String tmpName = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_NAME);
        String tmpAddress = PreferenceUtils.getStringValuePref(getApplicationContext(), PreferenceUtils.PREF_NAME, PreferenceUtils.PREF_KEY_ADDRESS);
        return (tmpName != null && tmpAddress != null);
    }

}
