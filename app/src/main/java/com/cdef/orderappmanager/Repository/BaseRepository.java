package com.cdef.orderappmanager.Repository;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.orderappmanager.Networking.Service;


/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BaseRepository {

    int refreshTokenAttemptCount = 0;


    protected MutableLiveData<Service> service = new MutableLiveData<>();

    public BaseRepository(Service service) {
        this.service.setValue(service);
    }


}
