package com.cdef.orderappmanager.Repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;


import com.cdef.orderappmanager.DataModel.Login.LoginData;
import com.cdef.orderappmanager.DataModel.OTPData;
import com.cdef.orderappmanager.Exceptions.CannotLoginExceptions;
import com.cdef.orderappmanager.Exceptions.NetworkExceptions;
import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Networking.Responses.LoginResponse;
import com.cdef.orderappmanager.Networking.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginRepository extends BaseRepository {

    public LoginRepository(Service service) {
        super(service);
    }

    public Observable<LoginData> login() {

        ///1. 로컬에 저장된 로그인 정보가 없는지 확인할것.

        ///2. 로컬에 없다면 서버에서 가져와야한다
        return this.service.getValue().login()
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(loginResponse -> {
                    LogUtil.d("로그인 중 flatMap : " + (Service.requestErrorHandler(loginResponse)));
                    if (Service.requestErrorHandler(loginResponse)) {
                        return Observable.just(loginResponse.result);
                    } else {
                        return Observable.error(new CannotLoginExceptions("cannot login"));
                    }
                });
    }

    public Observable<OTPData> getOTP(String url) {

        return this.service.getValue().getOTP(url)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .onErrorResumeNext(throwable -> Observable.error(throwable))
                .flatMap(otpData -> {
                    return Observable.just(otpData);
                });
    }


}
