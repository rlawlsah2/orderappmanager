package com.cdef.orderappmanager.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Utils.PreferenceUtils;
import com.cdef.orderappmanager.R;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.Data.DeviceInfo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FirebaseMsgService";

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        LogUtil.d("fcm 들어온다1 : " + remoteMessage);
        LogUtil.d("fcm 들어온다2 : " + remoteMessage.getMessageId());
        LogUtil.d("fcm 들어온다3 : " + remoteMessage.getData());
        LogUtil.d("fcm 들어온다4 : " + remoteMessage.getFrom());
        LogUtil.d("fcm 들어온다5 : " + remoteMessage.getNotification());
        for(String item : remoteMessage.getData().keySet())
        {
            LogUtil.d("fcm 들어온다5 : " + item);
        }


//        if(remoteMessage.getData() != null)
//        {
//            String versionCheck = remoteMessage.getData().get("versionCheck");
//
//            if(versionCheck != null)
//            {
//                checkOrderApp();
//                return;
//            }
//
//            String packageName = remoteMessage.getData().get("packageName");
//            String packageVersionCode = remoteMessage.getData().get("packageVersionCode");
//            String downloadUrl = remoteMessage.getData().get("downloadUrl");
//
//            if(packageName != null && downloadUrl != null) {
//
//                if(ManagerApplication.mContext != null)
//                    ((MainActivity)ManagerApplication.mContext).finish();
//
//                Intent intent = new Intent (this, MainActivity.class);
//                intent.putExtra("packageName", packageName);
//                intent.putExtra("downloadUrl", downloadUrl);
//                intent.putExtra("packageVersionCode", packageVersionCode);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//
//
//            }
//        }
        //추가한것
    }


    private void sendNotification(String messageBody) {

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("카운터")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX); //** MAX 나 HIGH로 줘야 가능함


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



    private void checkOrderApp()
    {
        String key = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            key = Utils.getMacAddrAPI6();
        } else {
            // do something for phones running an SDK before lollipop
            key = Utils.getMacAddress(this);

        }

//        key = key +  "(" + PreferenceUtils.getTableNoPref(this.mContext, "MACAOPOCHA" , "TableNo") + ")";

        String packageName = null;
//        if (BuildConfig.BRAND.equals("MACAOPOCHA")) {
//            packageName = "kr.cdefinition.electronicpayment";
//
//        } else if (BuildConfig.BRAND.equals("GYEONGSEONG")) {
//            packageName = "com.cdef.ordertablet";
//
//        }

        LogUtil.d("설치된 앱 목록 가져오기 대상: " + packageName);
        String appName = "";
        try {
            appName = (String) getPackageManager()

                    .getApplicationLabel(getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.updateTime = Utils.getDate("yyyy-MM-dd HH:mm:ss");
        deviceInfo.token = FirebaseInstanceId.getInstance().getToken();
//        deviceInfo.installedOrderApp = BuildConfig.BRAND;
        deviceInfo.tableNo = PreferenceUtils.getTableNoPref() + "";





        // 설치된 앱 목록 가져오기
        List<PackageInfo> pack = getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < pack.size(); i++) {
//            LogUtil.d("설치된 앱 목록 가져오기 : " + pack.get(i).packageName);

            if (pack.get(i) != null && pack.get(i).packageName != null) {
                if (pack.get(i).packageName.equals(packageName)) {
//                    LogUtil.d("@@@@@ 찾았다 설치된 앱 목록 가져오기 : " + pack.get(i).packageName);


                    ///디바이스 정보도 같이 넘겨줘라
                    deviceInfo.installedVersionCode = pack.get(i).versionCode + "";
                    deviceInfo.installedVersionName = pack.get(i).versionName;

                    break;
                }
            }
        }

//        FirebaseDatabase.getInstance().getReference().child(BuildConfig.BRAND).child(BuildConfig.BRANCH)
//                .child(key).setValue(deviceInfo);
    }
}
