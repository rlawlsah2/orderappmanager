package com.cdef.orderappmanager.fcm;
/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.cdef.orderappmanager.LogUtil;
import com.cdef.orderappmanager.Utils.PreferenceUtils;
import com.cdef.orderappmanager.Utils.Utils;
import com.cdef.orderappmanager.Data.DeviceInfo;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.List;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
//        String key = "";
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            key = Utils.getMacAddrAPI6();
//        } else {
//            // do something for phones running an SDK before lollipop
//            key = Utils.getMacAddress(this);
//
//        }
//
////        key = key +  "(" + PreferenceUtils.getTableNoPref(this.mContext, "MACAOPOCHA" , "TableNo") + ")";
//
//        String packageName = null;
////        if (BuildConfig.BRAND.equals("MACAOPOCHA")) {
////            packageName = "kr.cdefinition.electronicpayment";
////
////        } else if (BuildConfig.BRAND.equals("GYEONGSEONG")) {
////            packageName = "com.cdef.ordertablet";
////
////        }
//
//        LogUtil.d("설치된 앱 목록 가져오기 대상: " + packageName);
//        String appName = "";
//        try {
//            appName = (String) getPackageManager()
//
//                    .getApplicationLabel(getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES));
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        DeviceInfo deviceInfo = new DeviceInfo();
//        deviceInfo.updateTime = Utils.getDate("yyyy-MM-dd HH:mm:ss");
//        deviceInfo.token = FirebaseInstanceId.getInstance().getToken();
////        deviceInfo.installedOrderApp = BuildConfig.BRAND;
//        deviceInfo.tableNo = PreferenceUtils.getTableNoPref() + "";
//
//
//
//
//
//        // 설치된 앱 목록 가져오기
//        List<PackageInfo> pack = getPackageManager().getInstalledPackages(0);
//        for (int i = 0; i < pack.size(); i++) {
//
//            if (pack.get(i) != null && pack.get(i).packageName != null) {
//                if (pack.get(i).packageName.equals(packageName)) {
//
//                    ///디바이스 정보도 같이 넘겨줘라
//                    deviceInfo.installedVersionCode = pack.get(i).versionCode + "";
//                    deviceInfo.installedVersionName = pack.get(i).versionName;
//
//                    break;
//                }
//            }
//        }
    }


}