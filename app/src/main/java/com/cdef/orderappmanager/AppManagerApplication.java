package com.cdef.orderappmanager;

import android.app.Application;
import android.content.Context;

import com.cdef.orderappmanager.Networking.DaggerNetworkComponent;
import com.cdef.orderappmanager.Networking.NetworkComponent;
import com.cdef.orderappmanager.Networking.NetworkModule;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by kimjinmo on 2016. 11. 12..
 * 애플리케이션 전반적인 관리를 하는 클래스. 생명주기가 따로 존재함 참고바람
 */

public class AppManagerApplication extends Application {


    public static Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(false);


    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public NetworkComponent networkComponent = DaggerNetworkComponent.builder()
            .networkModule(new NetworkModule())
            .build();
    public static NetworkComponent getNetworkComponent(Context context)
    {
        return ((AppManagerApplication)context.getApplicationContext()).networkComponent;
    }

}
