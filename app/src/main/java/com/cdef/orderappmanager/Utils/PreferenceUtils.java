package com.cdef.orderappmanager.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.cdef.orderappmanager.LogUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;


/*
 *   Preference 기능에 대해 null일 경우에 대해 처리한 클래스
 *
 * */

public class PreferenceUtils {

    public static final String PREF_FILENAME_TABLESETTING = "pref_filename_tablesetting";
    public static final String PREF_KEY_TABLESETTING_TABLENO = "pref_key_tablesetting_tableno";
    public static final String PREF_KEY_TABLESETTING_TABLE_HALLNAME = "pref_key_tablesetting_table_hallname";
    public static final String PREF_KEY_TABLESETTING_LOGIN_BRANCH_ID = "pref_key_tablesetting_login_branch_id";
    public static final String PREF_KEY_TABLESETTING_REQUIREINSTALLLIST = "pref_key_tablesetting_require_install_list";
    public static final String PREF_KEY_TABLESETTING_DEFAULT_BACKGROUND_IMAGE = "pref_key_tablesetting_default_background_image";
    public static final String PREF_KEY_TABLESETTING_DEFAULT_STARTER_IMAGE = "pref_key_tablesetting_default_starter_image";

    public static final String PREF_KEY_NAME = "btmodule_key_name";
    public static final String PREF_KEY_ADDRESS = "btmodule_key_address";
    public static final String PREF_NAME = "btmodule_name";

    public static SharedPreferences getPreferences(Context context, String prefName) {
        SharedPreferences preference = null;
        if ((context == null) || (prefName == null) || ("".equals(prefName))) {
            return null;
        }
        preference = context.getSharedPreferences(prefName, 0);
        return preference;
    }

    public static void addStringValuePref(Context context, String prefName, String key, String value) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(key, value);
            edit.commit();
        }
    }

    public static void addBooleanValuePref(Context context, String prefName, String key, boolean value) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putBoolean(key, value);
            edit.commit();
        }
    }

    public static void addFloatValuePref(Context context, String prefName, String key, float value) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putFloat(key, value);
            edit.commit();
        }
    }

    public static void addLongValuePref(Context context, String prefName, String key, long value) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putLong(key, value);
            edit.commit();
        }
    }

    public static void addIntValuePref(Context context, String prefName, String key, int value) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putInt(key, value);
            edit.commit();
        }
    }

    public static String getStringValuePref(Context context, String prefName, String key) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getString(key, null);
        }
        return null;
    }

    public static boolean getBooleanValuePref(Context context, String prefName, String key) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getBoolean(key, false);
        }
        return false;
    }

    public static float getFloatValuePref(Context context, String prefName, String key) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getFloat(key, 0.0F);
        }
        return 0.0F;
    }

    public static long getLongValuePref(Context context, String prefName, String key) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getLong(key, 0L);
        }
        return 0L;
    }

    public static int getIntValuePref(Context context, String prefName, String key) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            return pref.getInt(key, 0);
        }
        return 0;
    }

    public static void clearPref(Context context, String prefName) {
        SharedPreferences pref = getPreferences(context, prefName);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.clear();
            edit.commit();
        }
    }

    public static void setStringGlobal(Context context, String prefName, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(prefName,
                Context.MODE_WORLD_READABLE);
        if (pref != null) {
            SharedPreferences.Editor edit = pref.edit();
            edit.putString(key, value);
            edit.commit();
        }
    }


    ////테이블 번호 관련

    public static SharedPreferences getPreferences(Context context, String prefName, int Mode) {
        SharedPreferences preference = null;
//        preference = context.getSharedPreferences("prefName", Context.MODE_WORLD_READABLE);


        if ((context == null) || (prefName == null) || ("".equals(prefName))) {
            return null;
        }
        preference = context.getSharedPreferences(prefName, Mode);

        return preference;
    }

    public static void addTableNoPref(Context context, String prefName, String key, String value) {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";

//        File file = new File(path);
//        if(!file.exists())
//            file.mkdirs();

        File savefile = new File(path + "TableNo.txt");
        LogUtil.d("테이블 번호 저장 시작 : " + savefile.getPath());
        BufferedWriter fw = null;

        try {
            fw = new BufferedWriter(new FileWriter(savefile));
            fw.write(value);
            LogUtil.d("테이블 번호 저장 완료 : " + fw);

        } catch (IOException e) {
            LogUtil.d("테이블 번호 저장 실패 : " + e);

        }
        // close file.
        if (fw != null) {
            // catch Exception here or throw.
            try {
                fw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public static String getTableNoPref() {

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/TableNo.txt");


        return readFile(file);


//
//        FileOutputStream outputStream;
//
//        try {
//            inputStream = context.openFileInput(Environment.DIRECTORY_DOWNLOADS + filename);
//            readFile
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /**
     * 파일 읽어 오기
     *
     * @param file
     */
    private static String readFile(File file) {

        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis));

            String content = "", temp = "";
            while ((temp = bufferedReader.readLine()) != null) {
                content += temp;
            }
            return content;
        } catch (Exception e) {
            return null;
        }


    }


}

