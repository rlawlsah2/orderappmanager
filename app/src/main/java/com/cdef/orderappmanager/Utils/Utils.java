package com.cdef.orderappmanager.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.cdef.orderappmanager.AppManagerApplication;
import com.cdef.orderappmanager.DataModel.Login.AppInfo;
import com.cdef.orderappmanager.LogUtil;

import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by kimjinmo on 2017. 10. 31..
 */

public class Utils {

    public static String getTiem() {
        return new Date().getTime() + "";
    }

    public static String getDate(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date());
    }

    public static String getMacAddress(Context context) {
        try {
            Context cntxt = context;
            WifiManager wifi = (WifiManager) cntxt.getSystemService(Context.WIFI_SERVICE);
            if (wifi == null) return "Failed: WiFiManager is null";

            WifiInfo info = wifi.getConnectionInfo();
            if (info == null) return "Failed: WifiInfo is null";

            return info.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Nothing";
    }

    public static String getMacAddrAPI6() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(Integer.toHexString(b & 0xFF) + ":");
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }


    /**
     * dp -> pixel 변환
     *
     * @param context
     * @param dp      dp값
     *                return int 픽셀로 변환된 값
     ***/
    public static int dpToPx(Context context, int dp) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float px = dp * (metrics.densityDpi / 160f);

        return (int) px;
    }

    /**
     * pixel -> dp 변환
     *
     * @param context
     * @param px      px 값
     *                return int dp로 변환된 값
     ***/
    public static int pxToDp(Context context, int px) {
        Resources resources = context.getResources();

        DisplayMetrics metrics = resources.getDisplayMetrics();

        float dp = px / (metrics.densityDpi / 160f);

        return (int) dp;


    }

    public static boolean isInstalled(Context context, String packageName) {

        // 설치된 앱 목록 가져오기
        List<PackageInfo> pack = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < pack.size(); i++) {

            if (pack.get(i) != null && pack.get(i).packageName != null) {
                if (pack.get(i).packageName.equals(packageName)) {
                    LogUtil.d("찾았다 : " + packageName);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 업데이트 혹은 설치가 필요한경우
     */
    public static boolean isNeedUpdate(AppInfo appInfo) {
        try {
            PackageInfo packageInfo = AppManagerApplication.mContext.getPackageManager().getPackageInfo(appInfo.pkgName, 0);
            ///깔려있음
            if (packageInfo != null) {
                int installedOrderAppVersion = packageInfo.versionCode;
                LogUtil.d("앱 체크 isNeedUpdate");
                LogUtil.d("앱 체크 isNeedUpdate installedOrderAppVersion : " + installedOrderAppVersion);
                LogUtil.d("앱 체크 isNeedUpdate appInfo.targetVersionCode : " + appInfo.targetVersionCode);
                if (appInfo.targetVersionCode > installedOrderAppVersion) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }


        } catch (PackageManager.NameNotFoundException e) {
            ///안깔려있으면 오류나더라
            return true;

        }
    }


    /**
     * 해당 패키지의 버전 정보 가져오기
     */
    public static PackageInfo getPackageInfo(String pkgName) {
        try {
            PackageInfo packageInfo = AppManagerApplication.mContext.getPackageManager().getPackageInfo(pkgName, 0);
            ///깔려있음
            if (packageInfo != null) {
                return packageInfo;
            } else {
                return null;
            }


        } catch (PackageManager.NameNotFoundException e) {
            ///안깔려있으면 오류나더라
            return null;

        }
    }

    public static int getWindosWidth(Context mContext) {

        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow().getAttributes();
        WindowManager wm = ((WindowManager) mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
        return (int) (wm.getDefaultDisplay().getWidth());
    }

    public static int getWindosHeight(Context mContext) {

        WindowManager.LayoutParams lp = ((Activity) mContext).getWindow().getAttributes();
        WindowManager wm = ((WindowManager) mContext.getApplicationContext().getSystemService(Context.WINDOW_SERVICE));
        return (int) (wm.getDefaultDisplay().getHeight());
    }
}
