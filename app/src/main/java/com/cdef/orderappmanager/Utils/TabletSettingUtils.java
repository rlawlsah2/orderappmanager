package com.cdef.orderappmanager.Utils;

import android.util.Base64;

import com.cdef.orderappmanager.AppManagerApplication;
import com.cdef.orderappmanager.DataModel.Login.AppInfo;
import com.cdef.orderappmanager.DataModel.Login.BranchInfo;
import com.cdef.orderappmanager.DataModel.Login.TableMap;
import com.cdef.orderappmanager.DataModel.Login.TableMapItem;
import com.cdef.orderappmanager.LogUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class TabletSettingUtils {

    private String accessToken = "";
    private String apiKey = ""; //주문앱 로그인할때 사용하는 키
    private String encodedEmail = "";
    private BranchInfo branchInfo = null;
    private static TabletSettingUtils instance;
    private ArrayList<TableMap> tableMaps;
    private HashMap<String, ArrayList<TableMapItem>> convertedTableMaps;

    public String getDefaultBackgroundImage() {
        return defaultBackgroundImage;
    }

    public void setDefaultBackgroundImage(String defaultBackgroundImage) {
        this.defaultBackgroundImage = defaultBackgroundImage;
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_DEFAULT_BACKGROUND_IMAGE, defaultBackgroundImage);
    }

    public String getDefaultStarterImage() {
        return defaultStarterImage;
    }

    public void setDefaultStarterImage(String defaultStarterImage) {
        this.defaultStarterImage = defaultStarterImage;
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_DEFAULT_STARTER_IMAGE, defaultStarterImage);

    }

    private String defaultBackgroundImage = null;
    private String defaultStarterImage = null;

    public HashMap<String, ArrayList<TableMapItem>> getConvertedTableMaps() {
        return convertedTableMaps;
    }

    public HashMap<String, AppInfo> getRequireAppList() {

        if (this.requireAppList == null) {
            String json = PreferenceUtils.getStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_REQUIREINSTALLLIST);
            Gson gson = new Gson();
            this.requireAppList = gson.fromJson(json, new TypeToken<HashMap<String, AppInfo>>() {
            }.getType());
        }
        return requireAppList;
    }

    public void setRequireAppList(HashMap<String, AppInfo> requireAppList) {
        if (this.requireAppList == null)
            this.requireAppList = new HashMap<>();
        this.requireAppList.clear();
        this.requireAppList.putAll(requireAppList);
        ////pref로 저장 (map -> json)
        Gson gson = new Gson();
        String convertedList = gson.toJson(requireAppList);
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_REQUIREINSTALLLIST, convertedList);
        gson = null;
    }

    private HashMap<String, AppInfo> requireAppList; ///서버에서 설치해야한다고 규정한 앱 목록.

    private String selectedTableNo = null;
    private String selectedTableHallName = null;

    public String getLoginBranchId() {

        if (this.loginBranchId == null) {
            loginBranchId = PreferenceUtils.getStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_LOGIN_BRANCH_ID);
        }
        return loginBranchId;
    }

    public void setLoginBranchId(String loginBranchId) {
        this.loginBranchId = loginBranchId;
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_LOGIN_BRANCH_ID, loginBranchId);
    }

    private String loginBranchId = null;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


    public String getEncodedEmail() {
        return encodedEmail;
    }

    public void setEncodedEmail(String email) {
        String result = null;
        try {
            result = new String(Base64.encode(email.getBytes("UTF-8"), Base64.DEFAULT)).trim();

        } catch (Exception e) {
            result = null;
            LogUtil.d("토큰 인코딩 에러 : " + e.getMessage());
        }
        this.encodedEmail = result;
    }


    public BranchInfo getBranchInfo() {
        return branchInfo;
    }

    public void setBranchInfo(BranchInfo branchInfo) {
        this.branchInfo = branchInfo;
    }


    public ArrayList<TableMap> getTableMaps() {
        return tableMaps;
    }

    public void setTableMaps(ArrayList<TableMap> tableMaps) {
        this.tableMaps = tableMaps;
        ///세부 분류 하자
        ///convertedTableMaps를 초기화한다.
        if (this.convertedTableMaps == null)
            this.convertedTableMaps = new HashMap<>();
        this.convertedTableMaps.clear();
        for (TableMap item : tableMaps) {
            this.convertedTableMaps.put(item.hallName, item.talbeMapJson);
        }
    }


    public void setSelectedTableNo(String tableNo) {
        this.selectedTableNo = tableNo;
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLENO, tableNo);
    }

    public String getSelectedTableNo() {
        if (this.selectedTableNo == null) {
            selectedTableNo = PreferenceUtils.getStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLENO);
        }
        return selectedTableNo;
    }


    public void setSelectedTableHallName(String tableHallName) {
        this.selectedTableHallName = tableHallName;
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLE_HALLNAME, tableHallName);
    }

    public String getSelectedTableHallName() {
        if (this.selectedTableHallName == null) {
            selectedTableHallName = PreferenceUtils.getStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLE_HALLNAME);
        }

        return selectedTableHallName;
    }

    public void clearAllLocalData() {
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLE_HALLNAME, null);
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLENO, null);
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_LOGIN_BRANCH_ID, null);
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_DEFAULT_BACKGROUND_IMAGE, null);
        PreferenceUtils.addStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_DEFAULT_STARTER_IMAGE, null);
    }


    synchronized public static TabletSettingUtils getInstacne() {
        if (TabletSettingUtils.instance == null) {
            TabletSettingUtils.instance = new TabletSettingUtils();
            TabletSettingUtils.instance.selectedTableNo = PreferenceUtils.getStringValuePref(AppManagerApplication.mContext, PreferenceUtils.PREF_FILENAME_TABLESETTING, PreferenceUtils.PREF_KEY_TABLESETTING_TABLENO);


        }

        return TabletSettingUtils.instance;
    }

    private TabletSettingUtils() {

    }


}
