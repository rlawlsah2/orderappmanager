package com.cdef.orderappmanager.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cdef.orderappmanager.View.Activity.MainActivity2;

public class BootReceiver extends BroadcastReceiver {


    @Override

    public void onReceive(Context context, Intent intent) {

        Intent myIntent = new Intent(context, MainActivity2.class);

        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(myIntent);

    }

}



